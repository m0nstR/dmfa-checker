# Checker for Various Acyclicity and Cyclicity Notions and Classifier for Rule Classes (e.g. guarded, sticky)

We provide a reference implementation of various checks for membership in certain rule set classes and (a)cyclicity notions (initially only [Disjunctive Model Faithful Acyclicity](https://iccl.inf.tu-dresden.de/web/Thema3509), 
which influenced the naming a lot).  
Beside `DMFA` our tool can also be used to check if a rule set is `MFA` or `RMFA`. 
Additionally, we support the cyclicity notions `MFC`, `DMFC`, `DRPC` (a fixed version of `RMFC`), and `RPC` (see [Usage](#usage) for details).
This simplifies comparison in the evaluations we want to perform.
(For historic reasons and ultimate confusion, `DRPC` is sometimes just called `RMFC` and `RPC` is sometimes called `DRMFC`. The original notion of `RMFC` is not implemented here at the moment.)

Our tool is based on the [Rulewerk](https://github.com/knowsys/rulewerk/) Library which in turn uses 
[VLog](https://github.com/karmaresearch/vlog) and [OWLAPI](https://github.com/owlcs/owlapi).
Since we want to be able to handle rules with disjunctions, we use our own [fork of Rulewerk](https://github.com/monsterkrampe/rulewerk).
For simplicity, this fork of rulewerk has been published to the [package registry](https://gitlab.com/m0nstR/dmfa-checker/-/packages) of this project.

Additionally we use [`kotlinx-cli`](https://github.com/Kotlin/kotlinx-cli) to build a very basic CLI for our project (see [Usage](#usage))
and [`kotlinx.coroutines`](https://github.com/Kotlin/kotlinx.coroutines) for basic multithreading.

We obtain test data by (normalizing and) translating existing OWL ontologies (see https://gitlab.com/m0nstR/owl-to-disjunctive-existential-rules-converter).

## Usage

Our tool reads the rule set from `STDIN` (in the default serialization format of Rulewerk) and outputs the check result to `STDOUT`.
The tool can be used to check the following acyclicity notions via subcommands:  

- `MFA` using `mfa`. This uses the `MFA` check that is already implemented in VLog and just replaces disjunctions in rules by conjunctions beforehand.
- `DMFA` using `dmfa`. This is our own implementation of `DMFA` (`MFA` with better support for disjunctions) that uses VLog only to compute Datalog closures of fact sets.
- `DMFA` with arbitrary cyclic term nesting depth using `dmfa -depth <depth>`.
- `RMFA` using `rmfa`. This is our own implementation of `RMFA` (with support for disjunctions) that uses VLog only to compute Datalog closures of fact sets.
- `RMFA` with arbitrary cyclic term nesting depth using `rmfa -depth <depth>`.
- `MFC` using `mfc`. This uses the `MFC` check that is already implemented in VLog and just drops rules with disjunctions beforehand. 
- `DMFC` using `dmfc`. This is our own implementation of `DMFC` (`MFC` with better support for disjunctions) that uses VLog only to compute Datalog closures of fact sets.
- `DRPC` using `drpc`. This is our own implementation of `DRPC` (a fixed version of `RMFC`) that uses VLog only to compute Datalog closures of fact sets.
- `RPC` using `rpc`. This is our own implementation of `RPC` (fixed `RMFC` with better support for disjunctions) that uses VLog only to compute Datalog closures of fact sets.
- A generic check there various improvement ideas for (non-)termination can be activated via flags. This is mostly meant for development purposes. Can be used via the subcommand `generic-chaselike`

**The tool also allows to check membership in other classes of rule sets (e.g. guarded and sticky) via the respective subcommands.** Just see `--help` to get the list of supported subcommands.

A bundled version of the application can be downloaded from the CI/CD Jobs. You obtain a `.zip` or `.tar` which contain an executable shell script and a `.bat` file as well.
You also find links to those in the [releases](https://gitlab.com/m0nstR/dmfa-checker/-/releases).

Assuming that the shell script is located at `./app/bin/app`, you can check a rule set as follows (you can also run `./app/bin/app --help` to get some more info):

```bash
cat <path-to-rule-set> | ./app/bin/app dmfa # result to STDOUT (and STDERR)
cat <path-to-rule-set> | JAVA_OPTS="-Xmx8g -XX:+UseConcMarkSweepGC" ./app/bin/app dmfa # custom options for JVM
cat <path-to-rule-set> | ./app/bin/app -t dmfa >check-result 2>check-error-log # writes result and error to files
```

For convenience, we also offer a docker image `registry.gitlab.com/m0nstr/dmfa-checker` in our [container registry](https://gitlab.com/m0nstR/dmfa-checker/container_registry).
These images are also tagged with the appropriate version numbers.

```bash
cat <path-to-rule-set> | docker run --rm -i registry.gitlab.com/m0nstr/dmfa-checker dmfa # outputs results to STDOUT (and STDERR)
cat <path-to-rule-set> | docker run --rm -e JAVA_OPTS="-Xmx8g -XX:+UseConcMarkSweepGC" -i registry.gitlab.com/m0nstr/dmfa-checker dmfa # custom options for JVM
cat <path-to-rule-set> | docker run --rm -i registry.gitlab.com/m0nstr/dmfa-checker dmfa >check-result 2>check-error-log # writes result and error to files
```

## Tests

The `test-data` directory contains various rule sets with that we automatically test our implementation. 
For example, the `isDmfa` subdirectory
contains all rule sets (or symlinks to them) that should be `DMFA`.
The tests are run by CI Jobs for every commit.
You can run the tests locally with `./gradlew test`.
Also, you can simply add more test cases by adding rule sets to the appropriate subdirectories.


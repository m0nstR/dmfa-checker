FROM openjdk:8-jre

WORKDIR /app

# tar unpacking is intentional
ADD app/build/distributions/app.tar /app/

ENTRYPOINT ["./app/bin/app"]

checkNotion() {
	file=$1
	notion=$2
    cliOptions=$3

	#if ! grep -q "$notion: [tf]" $file.result; then
	if ! grep -q "$notion:" $file.result; then
        echo -n "$notion: " >> $file.result
		cat $file | timeout 4h docker run --rm -e JAVA_OPTS="-Xmx16g -XX:+UseConcMarkSweepGC" -i registry.gitlab.com/m0nstr/dmfa-checker $cliOptions -d >> $file.result 2>>$file.log;
		exit_code=$?
		if [ $exit_code -eq 124 ]
		then
			echo "timeout $notion" >> $file.error;
			#echo "" >> $file.result

			#exit $exit_code
		elif [ $exit_code -ne 0 ]
		then
			echo "non-zero exit code $notion: $exit_code" >> $file.error;
			#echo "" >> $file.result
			#exit $exit_code
		fi
	fi
}


file=$1

# skip rulesets without existentials rulesets
# if [[ $(grep -c '!' $file) -eq 0 && $(grep -c '|' $file) -eq 0 ]]; then
if [[ $(grep -c '!' $file) -eq 0 ]]; then
	exit 0;
fi

# write metadata to result file
if [ ! -f $file.result ]; then
	echo "$file:" > $file.result;
	echo "Exis: $(grep -c '!' $file); Disj: $(grep -c '|' $file)" >> $file.result;
	echo "$file:" > $file.error;
fi

#checkNotion $file "mfaClassic" "-t termination -cv skolem"
#checkNotion $file "mfaDisjReplaced" "-t termination -cv skolem -disjToConj"
#checkNotion $file "dmfaClassic" "-t termination -cv skolem -disj"
#checkNotion $file "dmfaNoDLClosure" "-t termination -cv skolem -disj -blkNoDlgCl"
#checkNotion $file "dmfaNestingDepth2" "-t termination -cv skolem -disj -depth 2"
#checkNotion $file "dmfaND2NoDLClosure" "-t termination -cv skolem -disj -depth 2 -blkNoDlgCl"
checkNotion $file "rmfaClassic" "-t termination -cv restricted -disj"
checkNotion $file "rmfaNestingDepth2" "-t termination -cv restricted -disj -depth 2"

#checkNotion $file "mfcClassic" "-t non_termination -cv skolem"

# we do this as an upper bound
#checkNotion $file "mfcDisjReplaced" "-t non_termination -cv skolem -disjToConj"

#checkNotion $file "dmfcEquality" "-tmfb 100 -t non_termination -cv skolem -disj -uac equivalence_only_result"
#checkNotion $file "dmfcWithRuleBodiesEquality" "-tmfb 100 -t non_termination -cv skolem -disj -uac equivalence_only_result -ublkInclBodies"
#checkNotion $file "dmfcUniqueConstantsEquality" "-tmfb 100 -t non_termination -cv skolem -disj -uc -uac equivalence_only_result"
#checkNotion $file "dmfcWithRuleBodiesUniqueConstantsEquality" "-tmfb 100 -t non_termination -cv skolem -disj -uc -uac equivalence_only_result -ublkInclBodies"
#checkNotion $file "dmfcSubset" "-tmfb 100 -t non_termination -cv skolem -disj -uac subset"
#checkNotion $file "dmfcWithRuleBodiesSubset" "-tmfb 100 -t non_termination -cv skolem -disj -uac subset -ublkInclBodies"
#checkNotion $file "dmfcUniqueConstantsSubset" "-tmfb 100 -t non_termination -cv skolem -disj -uc -uac subset"
#checkNotion $file "dmfcWithRuleBodiesUniqueConstantsSubset" "-tmfb 100 -t non_termination -cv skolem -disj -uc -uac subset -ublkInclBodies"
#checkNotion $file "dmfcInter" "-tmfb 100 -t non_termination -cv skolem -disj -uac interchangeability"
#checkNotion $file "dmfcUniqueConstantsInter" "-tmfb 100 -t non_termination -cv skolem -disj -uc -uac interchangeability"

checkNotion $file "rmfcEquality" "-tmfb 100 -t non_termination -cv restricted -uac equivalence_including_rule"
#checkNotion $file "rmfcWithRuleBodiesEquality" "-tmfb 100 -t non_termination -cv restricted -uac equivalence_including_rule -ublkInclBodies"
#checkNotion $file "rmfcUniqueConstantsEquality" "-tmfb 100 -t non_termination -cv restricted -uc -uac equivalence_including_rule"
#checkNotion $file "rmfcWithRuleBodiesUniqueConstantsEquality" "-tmfb 100 -t non_termination -cv restricted -uc -uac equivalence_including_rule -ublkInclBodies"

# we do this as an upper bound
#checkNotion $file "rmfcDisjReplaced" "-tmfb 100 -t non_termination -cv restricted -uac equivalence_including_rule -disjToConj"

#checkNotion $file "drmfcWithRuleBodiesEquality" "-tmfb 100 -t non_termination -cv restricted -disj -uac equivalence_including_rule -ublkInclBodies"
#checkNotion $file "drmfcWithRuleBodiesUniqueConstantsEquality" "-tmfb 100 -t non_termination -cv restricted -disj -uc -uac equivalence_including_rule -ublkInclBodies"
#checkNotion $file "drmfcSubset" "-tmfb 100 -t non_termination -cv restricted -disj -uac subset"
#checkNotion $file "drmfcUniqueConstantsSubset" "-tmfb 100 -t non_termination -cv restricted -disj -uc -uac subset"
checkNotion $file "drmfcUniqueConstantsInter" "-tmfb 100 -t non_termination -cv restricted -disj -uc -uac interchangeability"
checkNotion $file "drmfcIgnoringRedundanciesInUnblkUniqueConstantsInter" "-tmfb 100 -t non_termination -cv restricted -disj -uc -uac interchangeability -ublkIgnoreRedundant"

#feature toggle checks 
checkNotion $file "rmfcIgnoringRedundanciesInUnblkEquality" "-tmfb 100 -t non_termination -cv restricted -uac equivalence_including_rule -ublkIgnoreRedundant"
checkNotion $file "rmfcUniqueConstantsEquality" "-tmfb 100 -t non_termination -cv restricted -uc -uac equivalence_including_rule"
checkNotion $file "rmfcInter" "-tmfb 100 -t non_termination -cv restricted -uac interchangeability"
checkNotion $file "drmfcEquality" "-tmfb 100 -t non_termination -cv restricted -disj -uac equivalence_including_rule"
checkNotion $file "drmfcIgnoringRedundanciesInUnblkEquality" "-tmfb 100 -t non_termination -cv restricted -disj -uac equivalence_including_rule -ublkIgnoreRedundant"
checkNotion $file "drmfcUniqueConstantsEquality" "-tmfb 100 -t non_termination -cv restricted -disj -uc -uac equivalence_including_rule"
checkNotion $file "drmfcInter" "-tmfb 100 -t non_termination -cv restricted -disj -uac interchangeability"
checkNotion $file "originalRmfcUniqueConstantsInter" "-tmfb 100 -t non_termination -cv restricted -uc -uac interchangeability"
checkNotion $file "originalRmfcIgnoringRedundanciesInUnblkUniqueConstantsInter" "-tmfb 100 -t non_termination -cv restricted -uc -uac interchangeability -ublkIgnoreRedundant"
checkNotion $file "drmfcUniqueConstantsOutputEquality" "-tmfb 100 -t non_termination -cv restricted -disj -uc -uac equivalence_only_result"
#checkNotion $file "drmfcOutputEquality" "-tmfb 100 -t non_termination -cv restricted -disj -uac equivalence_only_result"
checkNotion $file "drmfcIgnoringRedundanciesInUnblkUniqueConstantsOutputEquality" "-tmfb 100 -t non_termination -cv restricted -disj -uc -uac equivalence_only_result -ublkIgnoreRedundant"
#checkNotion $file "drmfcIgnoringRedundanciesInUnblkOutputEquality" "-tmfb 100 -t non_termination -cv restricted -disj -uac equivalence_only_result -ublkIgnoreRedundant"

#checkNotion $file "dmfcEqualityUblkDisjToConj" "-tmfb 100 -t non_termination -cv skolem -disj -ublkDisjToConj"
#checkNotion $file "dmfcWithRuleBodiesEqualityUblkDisjToConj" "-tmfb 100 -t non_termination -cv skolem -disj -ublkInclBodies -ublkDisjToConj"
#checkNotion $file "dmfcUniqueConstantsEqualityUblkDisjToConj" "-tmfb 100 -t non_termination -cv skolem -disj -uc -ublkDisjToConj"
#checkNotion $file "dmfcWithRuleBodiesUniqueConstantsEqualityUblkDisjToConj" "-tmfb 100 -t non_termination -cv skolem -disj -uc -ublkInclBodies -ublkDisjToConj"
#checkNotion $file "dmfcSubsetUblkDisjToConj" "-tmfb 100 -t non_termination -cv skolem -disj -ublkSubsetCheck -ublkDisjToConj"
#checkNotion $file "dmfcWithRuleBodiesSubsetUblkDisjToConj" "-tmfb 100 -t non_termination -cv skolem -disj -ublkSubsetCheck -ublkInclBodies -ublkDisjToConj"
#checkNotion $file "dmfcUniqueConstantsSubsetUblkDisjToConj" "-tmfb 100 -t non_termination -cv skolem -disj -uc -ublkSubsetCheck -ublkDisjToConj"
#checkNotion $file "dmfcWithRuleBodiesUniqueConstantsSubsetUblkDisjToConj" "-tmfb 100 -t non_termination -cv skolem -disj -uc -ublkSubsetCheck -ublkInclBodies -ublkDisjToConj"
#checkNotion $file "dmfcInterUblkDisjToConj" "-tmfb 100 -t non_termination -cv skolem -disj -inter -ublkDisjToConj"
#checkNotion $file "dmfcUniqueConstantsInterUblkDisjToConj" "-tmfb 100 -t non_termination -cv skolem -disj -uc -inter -ublkDisjToConj"

#checkNotion $file "drmfcEqualityUblkDisjToConj" "-tmfb 100 -t non_termination -cv restricted -disj -ublkDisjToConj"
#checkNotion $file "drmfcWithRuleBodiesEqualityUblkDisjToConj" "-tmfb 100 -t non_termination -cv restricted -disj -ublkInclBodies -ublkDisjToConj"
#checkNotion $file "drmfcUniqueConstantsEqualityUblkDisjToConj" "-tmfb 100 -t non_termination -cv restricted -disj -uc -ublkDisjToConj"
#checkNotion $file "drmfcWithRuleBodiesUniqueConstantsEqualityUblkDisjToConj" "-tmfb 100 -t non_termination -cv restricted -disj -uc -ublkInclBodies -ublkDisjToConj"
#checkNotion $file "drmfcSubsetUblkDisjToConj" "-tmfb 100 -t non_termination -cv restricted -disj -ublkSubsetCheck -ublkDisjToConj"
#checkNotion $file "drmfcUniqueConstantsSubsetUblkDisjToConj" "-tmfb 100 -t non_termination -cv restricted -disj -uc -ublkSubsetCheck -ublkDisjToConj"
#checkNotion $file "drmfcInterUblkDisjToConj" "-tmfb 100 -t non_termination -cv restricted -disj -inter -ublkDisjToConj"
#checkNotion $file "drmfcUniqueConstantsInterUblkDisjToConj" "-tmfb 100 -t non_termination -cv restricted -disj -uc -inter -ublkDisjToConj"


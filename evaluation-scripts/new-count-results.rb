#!/usr/bin/env ruby

require 'set'

# directory with result files
path = ARGV[0]
only_non_disjunctive = true

combined_results = Hash.new(0)

all_keys = Set.new

Dir.glob("#{path}/*.result")
.select { |file|
  f = File.open(file)
  _first_line = f.readline
  second_line = f.readline.strip
  !only_non_disjunctive || /; ?Disj: 0$/.match?(second_line)
}
.each { |file|
  key = Hash[File.open(file).each_line.drop(2).map { |l|
    l.gsub(/\s+/, "").split(':')
  }.filter { |pair| pair.length == 2 }]

  # hack for rerun of weakly-sticky
  File.open("#{file}.onlyweaklysticky").each_line.drop(1).each { |l|
    pair = l.gsub(/\s+/, "").split(':')
    key[pair[0]] = pair[1]
  }

  # hack for rerun of domain-restricted
  File.open("#{file}.onlydomainrestricted").each_line.drop(1).each { |l|
    pair = l.gsub(/\s+/, "").split(':')
    key[pair[0]] = pair[1]
  }

  # hack for run of warded
  File.open("#{file}.onlywarded").each_line.drop(1).each { |l|
    pair = l.gsub(/\s+/, "").split(':')
    key[pair[0]] = pair[1]
  }

  all_keys |= key.keys.to_set

  combined_results[key] += 1
}

all_keys_list = all_keys.to_a

puts "#{all_keys_list.join(",")},count"

combined_results.each { |k, v|
  puts "#{all_keys_list.map { |notion| k[notion] }.join(",")},#{v}"
}


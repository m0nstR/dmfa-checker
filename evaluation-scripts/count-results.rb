#!/usr/bin/env ruby

CHECKED_NOTIONS = [
  #:mfaClassic,
  #:mfaDisjReplaced,
  #:dmfaClassic,
  #:dmfaNoDLClosure,
  #:dmfaNestingDepth2,
  #:dmfaND2NoDLClosure,
  #:rmfaClassic,
  :rmfaNestingDepth2,
  #:mfcClassic,
  #:mfcDisjReplaced,
  #:dmfcEquality,
  #:dmfcWithRuleBodiesEquality,
  #:dmfcUniqueConstantsEquality,
  #:dmfcWithRuleBodiesUniqueConstantsEquality,
  #:dmfcSubset,
  #:dmfcWithRuleBodiesSubset,
  #:dmfcUniqueConstantsSubset,
  #:dmfcWithRuleBodiesUniqueConstantsSubset,
  #:dmfcInter,
  #:dmfcUniqueConstantsInter,
  :rmfcEquality,
  #:rmfcWithRuleBodiesEquality,
  #:rmfcUniqueConstantsEquality,
  #:rmfcWithRuleBodiesUniqueConstantsEquality,
  #:rmfcDisjReplaced,
  #:drmfcEquality,
  #:drmfcWithRuleBodiesEquality,
  #:drmfcUniqueConstantsEquality,
  #:drmfcWithRuleBodiesUniqueConstantsEquality,
  #:drmfcInter,
  #:drmfcUniqueConstantsInter,
  #:drmfcIgnoringRedundanciesInUnblkUniqueConstantsInter,
  #:dmfcEqualityUblkDisjToConj,
  #:dmfcWithRuleBodiesEqualityUblkDisjToConj,
  #:dmfcUniqueConstantsEqualityUblkDisjToConj,
  #:dmfcWithRuleBodiesUniqueConstantsEqualityUblkDisjToConj,
  #:dmfcSubsetUblkDisjToConj,
  #:dmfcWithRuleBodiesSubsetUblkDisjToConj,
  #:dmfcUniqueConstantsSubsetUblkDisjToConj,
  #:dmfcWithRuleBodiesUniqueConstantsSubsetUblkDisjToConj,
  #:dmfcInterUblkDisjToConj,
  #:dmfcUniqueConstantsInterUblkDisjToConj,
  #:drmfcEqualityUblkDisjToConj,
  #:drmfcWithRuleBodiesEqualityUblkDisjToConj,
  #:drmfcUniqueConstantsEqualityUblkDisjToConj,
  #:drmfcWithRuleBodiesUniqueConstantsEqualityUblkDisjToConj,
  #:drmfcInterUblkDisjToConj,
  #:drmfcUniqueConstantsInterUblkDisjToConj,
]

OPTIONAL_NOTIONS = [
  :rmfcIgnoringRedundanciesInUnblkEquality,
  #:rmfcUniqueConstantsEquality,
  #:rmfcInter,
  #:rmfcUniqueConstantsInter,
  #:rmfcIgnoringRedundanciesInUnblkUniqueConstantsInter,
  #:drmfcEquality,
  #:drmfcIgnoringRedundanciesInUnblkEquality,
  :drmfcUniqueConstantsEquality,
  #:drmfcInter,
  #:originalRmfcUniqueConstantsInter,
  #:originalRmfcIgnoringRedundanciesInUnblkUniqueConstantsInter,
  #:drmfcUniqueConstantsOutputEquality,
  #:drmfcOutputEquality,
  :drmfcIgnoringRedundanciesInUnblkUniqueConstantsOutputEquality,
  #:drmfcIgnoringRedundanciesInUnblkOutputEquality
]

# calling this without a target_notion counts the rule sets for that each check has been run (possibly resulting in a timeout)
# calling this a single target_notion counts the rule sets for that each check has been run (possibly resulting in a timeout) and for that the target notion is true
# calling this more than one target_notion counts the rule sets for that each check has been run (possibly resulting in a timeout) and for that each target notion is false (counting open cases)
def generate_grep_notion_substring(target_notions)
  if target_notions.size <= 1
    ".*#{CHECKED_NOTIONS.map{ |n| target_notions.include?(n) ? "[^a-z]#{n}: true" : "[^a-z]#{n}:" }.join(".*")}#{OPTIONAL_NOTIONS.map{ |n| target_notions.include?(n) ? ".*[^a-z]#{n}: true" : ""}.join("")}"
  else
    ".*#{CHECKED_NOTIONS.map{ |n| target_notions.include?(n) ? "[^a-z]#{n}: false" : "[^a-z]#{n}:" }.join(".*")}"
  end
end

EXIS_RANGES = [
  #:from1to9,
  #:from10to19,
  :from1to19,
  #:from20to39,
  #:from40to99,
  :from20to99,
  #:from100to299,
  #:from100toInfty,
  :from100to999,
  #:from1000toInfty,
  #:from1toInfty,
  #:from1to99,
]

DISJ_RANGES = [
  #:from0to0,
  #:from1to9,
  #:from10to19,
  #:from20to99,
  #:from100toInfty,
  :from1toInfty,
  #:from0toInfty,
]

# set to true to consider only non-guarded rule sets
NON_GUARDED_ONLY = false

def generate_grep_number_range_substring(range)
  case range
  when :from0to0
    "0"
  when :from1to9
    "[1-9]"
  when :from10to19
    "1[0-9]"
  when :from1to19
    "([1-9]|1[0-9])"
  when :from20to39
    "[2-3][0-9]"
  when :from20to99
    "[2-9][0-9]"
  when :from40to99
    "[4-9][0-9]"
  when :from10to99
    "[1-9][0-9]"
  when :from1to99
    "[1-9][0-9]{0,1}"
  when :from100to299
    "[1-2][0-9]{2}"
  when :from100to999
    "[1-9][0-9]{2}"
  when :from100toInfty
    "[1-9][0-9]{2,}"
  when :from1000toInfty
    "[1-9][0-9]{3,}"
  when :from1toInfty
    "[1-9][0-9]{0,}"
  when :from0toInfty
    "[0-9]{1,}"
  end
end

def call_grep(exis_range, disj_range, notion)
  files_fulfilling_notion = `grep -Ezl "Exis: #{generate_grep_number_range_substring(exis_range)}; Disj: #{generate_grep_number_range_substring(disj_range)}#{notion == :unfinished ? "" : generate_grep_notion_substring(notion)}" ./translated-files/*.result`.lines

  if NON_GUARDED_ONLY
    files_fulfilling_notion.select do |filename|
      rules_filename = filename[/^.*\.rules/]
      `grep -El ":-.*\\(\\?X0, \\?X1\\).*\\(\\?X1, \\?X2\\)" #{rules_filename}`.strip == rules_filename
    end.length
  else
    files_fulfilling_notion.length
  end
end

# puts "\t\t" + DISJ_RANGES.join("\t\t\t")
EXIS_RANGES.each do |er|
  puts "& #{er} & " + (
    DISJ_RANGES
      .map do |dr|
        "#{call_grep(er, dr, :unfinished)} & #{call_grep(er, dr, [])} & " + # called without notion to count how many produced results at all
          (CHECKED_NOTIONS + OPTIONAL_NOTIONS)
            .map do |n|
              call_grep(er, dr, [n])
            end
              .join(" & ") #+
          #" & #{call_grep(er, dr, [:rmfaClassic, :rmfcEquality])}" +
          #" & #{call_grep(er, dr, [:rmfaNestingDepth2, :drmfcUniqueConstantsInter])}"
      end
    )
      .join("") + "\\\\"
    #.join("\t")
end

total_finished = EXIS_RANGES.sum do |er| 
  DISJ_RANGES.sum do |dr| 
    call_grep(er, dr, [])
  end
end
total = EXIS_RANGES.sum do |er| 
  DISJ_RANGES.sum do |dr| 
    call_grep(er, dr, :unfinished)
  end
end

#open_rmfa_rmfc = EXIS_RANGES.sum do |er|
#  DISJ_RANGES.sum do |dr|
#    call_grep(er, dr, [:rmfaClassic, :rmfcEquality])
#  end
#end

open_rmfa2_rmfc = EXIS_RANGES.sum do |er|
  DISJ_RANGES.sum do |dr|
    call_grep(er, dr, [:rmfaNestingDepth2, :rmfcEquality])
  end
end

#open_rmfa2_drmfc = EXIS_RANGES.sum do |er|
#  DISJ_RANGES.sum do |dr|
#    call_grep(er, dr, [:rmfaNestingDepth2, :drmfcUniqueConstantsInter])
#  end
#end


puts "& \\multirow{2}{*}{\\bf 1+} & \\multirow{2}{*}{\\bf #{total}} & \\multirow{2}{*}{\\bf #{total_finished}} & " + ((CHECKED_NOTIONS + OPTIONAL_NOTIONS)
  .map do |n|
    notion_count = EXIS_RANGES.sum do |er| 
      DISJ_RANGES.sum do |dr| 
        call_grep(er, dr, [n])
      end
    end

    "\\bf #{notion_count}"
  end
    .join(" & ")) +
    #" & #{open_rmfa_rmfc}" +
    " & #{open_rmfa2_rmfc}" +
    #" & #{open_rmfa2_drmfc}" +
    "\\\\"
# called without notion to count how many produced results at all

puts "& & & & " + ((CHECKED_NOTIONS + OPTIONAL_NOTIONS)
  .map do |n|
    notion_count = EXIS_RANGES.sum do |er| 
      DISJ_RANGES.sum do |dr| 
        call_grep(er, dr, [n])
      end
    end

    "\\bf (#{notion_count * 100 / total_finished}\\%)"
  end
    .join(" & ")) #+
    #" & #{open_rmfa_rmfc * 100 / total_finished}\\%" +
    #" & #{open_rmfa2_drmfc * 100 / total_finished}\\%"
# called without notion to count how many produced results at all

puts ""

#puts "& 100+ & " + (DISJ_RANGES
#  .map do |dr|
#    "#{call_grep(:from100toInfty, dr, [])} (#{call_grep(:from100toInfty, dr, :unfinished)}) & " +
#      CHECKED_NOTIONS
#        .map do |n|
#          call_grep(:from100toInfty, dr, n)
#        end
#          .join(" & ")         # called without notion to count how many produced results at all
#  end)
#    .join("")
  #.join("\t")

  puts "\n\n\n"

  (CHECKED_NOTIONS + OPTIONAL_NOTIONS)
    .each do |n|
      notion_count = EXIS_RANGES.sum do |er|
        DISJ_RANGES.sum do |dr|
          call_grep(er, dr, [n])
        end
      end

      puts "#{n}: #{notion_count}"
    end


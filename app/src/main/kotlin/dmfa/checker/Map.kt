package dmfa.checker

/*
    DMFA Checker
    Map

    Copyright 2023 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

// adds pair with set to map by adding to the inner collection if the key already exists
@JvmName("mergeWithSetValues")
fun <K, V>Map<K, Set<V>>.merge(other: Pair<K, Set<V>>): Map<K, Set<V>> =
    this + Pair(other.first, getOrDefault(other.first, setOf()) + other.second)

// adds pairs with sets to map by adding to the inner collection if the key already exists
@JvmName("mergeWithSetValues")
fun <K, V>Map<K, Set<V>>.merge(other: Iterable<Pair<K, Set<V>>>): Map<K, Set<V>> =
    other.fold(this) { acc, p -> acc.merge(p) }

// adds pair to map by adding to the inner collection if the key already exists
fun <K, V>Map<K, Set<V>>.merge(other: Pair<K, V>): Map<K, Set<V>> =
    merge(Pair(other.first, setOf(other.second)))

// adds pairs to map by adding to the inner collection if the key already exists
fun <K, V>Map<K, Set<V>>.merge(other: Iterable<Pair<K, V>>): Map<K, Set<V>> =
    other.fold(this) { acc, p -> acc.merge(p) }

// combines entries of the two maps by adding to the inner collection if the key already exists
fun <K, V>Map<K, Set<V>>.merge(other: Map<K, Set<V>>): Map<K, Set<V>> =
    merge(other.map { it.toPair() })

// creates new map that contains only the values that are in the first map but not in the second; if a key is in both, the sets are subtracted
fun <K, V>Map<K, Set<V>>.subtract(other: Map<K, Set<V>>): Map<K, Set<V>> =
    map { (k, v) ->
        if (!other.containsKey(k)) {
            Pair(k, v)
        } else {
            val newSet = v - other.get(k)!!

            if (newSet.isEmpty()) {
                null
            } else {
                Pair(k, newSet)
            }
        }
    }.filterNotNull().toMap()

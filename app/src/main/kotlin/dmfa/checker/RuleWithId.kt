package dmfa.checker

/*
    DMFA Checker
    RuleWithId

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.semanticweb.rulewerk.core.model.api.ExistentialVariable
import org.semanticweb.rulewerk.core.model.api.Fact
import org.semanticweb.rulewerk.core.model.api.Predicate
import org.semanticweb.rulewerk.core.model.api.Rule
import org.semanticweb.rulewerk.core.model.api.UniversalVariable
import org.semanticweb.rulewerk.core.model.implementation.Expressions
import kotlin.streams.toList

fun Rule.toRuleWithId(id: Int) = RuleWithId(this, id)

val Rule.isDisjunctiveExistential: Boolean
    get() = body.conjunctions.size == 1 && body.existentialVariables.toList().size == 0

val Rule.isDeterministic: Boolean
    get() = isDisjunctiveExistential && head.conjunctions.size == 1

val Rule.isDatalog: Boolean
    get() = isDeterministic && existentialVariables.toList().size == 0

val Rule.isGenerating: Boolean
    get() = isDisjunctiveExistential && head.existentialVariables.toList().size > 0

val Rule.frontier: List<UniversalVariable>
    get() = body.universalVariables.toList().filter { it in head.universalVariables.toList() }.distinct()

fun Rule.disjunctionsToConjunctions(): Rule =
    Expressions.makeRule(
        Expressions.makeDisjunction(
            listOf(
                head.conjunctions.flatMap { conjunction ->
                    conjunction.literals
                }
            )
        ),
        body
    )

// TODO: check if joins within the same atom are allowed! (currently they are not)
val Rule.isJoinless: Boolean get() = body.conjunctions.all { conj ->
    conj.universalVariables.toList().toSet().all { variable ->
        val varCount = conj.sumOf { it.arguments.count { it == variable } }
        if (varCount == 0) { throw Exception("variable $variable should occur in body but the count is zero... something is wrong!!!") }

        varCount == 1
    }
}

val Rule.isLinear: Boolean get() = body.conjunctions.all { it.count() == 1 }

val Rule.isGuarded: Boolean get() = body.conjunctions.all { it.isGuardedForVars(it.universalVariables.toList().toSet()) }

val Rule.joinVariables: Set<UniversalVariable> get() = body.conjunctions.fold(setOf()) { acc, conj ->
    val duplicateVars: Set<UniversalVariable> = conj.flatMap { it.arguments.filterIsInstance<UniversalVariable>() }.groupingBy { it }.eachCount().filter { it.value > 1 }.keys
    acc + duplicateVars
}

// returning null means that the rule is already violating the sticky requirement (i.e. a join variable does not occur in every head atom)
fun Rule.stickyHeadPositionsForBodyVariables(variables: Set<UniversalVariable>): Map<Predicate, Set<Int>>? {
    return head.conjunctions.fold(mapOf()) { acc, conj ->
        val atoms = conj.literals

        variables.fold(acc) { innerAcc, variable ->
            // if there is some head atom where the variable does not occur, the sticky condition is violated
            if (atoms.any { it.arguments.all { it != variable } }) {
                return null
            }

            val varPositions = conj.positionsOfVariable(variable)

            innerAcc.merge(varPositions)
        }
    }
}

val Rule.isDomainRestricted: Boolean get() = body.conjunctions.all { bodyConj ->
    val bodyVars = bodyConj.universalVariables.toList()
    head.conjunctions.all { headConj ->
        headConj.all { headAtom ->
            val headVars = headAtom.universalVariables.toList()
            headVars.isEmpty() || headVars.containsAll(bodyVars)
        }
    }
}

val Rule.isFrontierOne: Boolean get() = frontier.count() <= 1

// TODO: paper mentiones this is frontier one; I do not see why...
val Rule.isMonadic: Boolean get() = head.conjunctions.all { it.literals.all { it.arguments.count() == 1 } }

val Rule.isFrontierGuarded: Boolean get() = body.conjunctions.all { it.isGuardedForVars(frontier.toSet()) }

// NOTE: very similar to sticky but we do not abort if a variable does not occur in a head conjunction
fun Rule.affectedPositionsForBodyVariables(variables: Set<UniversalVariable>): Map<Predicate, Set<Int>> {
    return head.conjunctions.fold(mapOf()) { acc, conj ->
        variables.fold(acc) { innerAcc, variable ->
            val varPositions = conj.positionsOfVariable(variable)
            innerAcc.merge(varPositions)
        }
    }
}

val Rule.affectedPositionsBase: Map<Predicate, Set<Int>> get() {
    return head.conjunctions.fold(mapOf()) { acc, conj ->
        conj.existentialVariables.toList().fold(acc) { innerAcc, variable ->
            val varPositions = conj.positionsOfVariable(variable)
            innerAcc.merge(varPositions)
        }
    }
}

fun Rule.isWardedForPositions(positions: Map<Predicate, Set<Int>>): Boolean {
    return body.conjunctions.all { conj ->
        val harmfulVars = conj.variablesOnlyAtPositions(positions)
        val dangerousVars = head.universalVariables.toList().toSet().intersect(harmfulVars)

        dangerousVars.isEmpty() || (
            conj.literals.any { lit ->
                val vars = lit.universalVariables.toList().toSet()

                vars.containsAll(dangerousVars) && // and every harmful variable is either not in lit or (if it is in lit) is not in any other literal in conj (i.e. lit is the only literal with the variable)
                    harmfulVars.all { v -> !vars.contains(v) || (conj.literals.count { it.universalVariables.toList().contains(v) } <= 1) }
            }
            )
    }
}

class RuleWithId(val rule: Rule, val id: Int, val disjunctIndex: Int? = null) : Rule by rule {
    fun bodyForAssignment(assignment: Assignment): List<Fact> {
        return body.conjunctions.flatMap {
            // false is fine here, since all variables should be universally quantified in body anyway
            it.forAssignment(assignment, false)
        }
    }

    fun headDisjunctsForAssignment(assignment: Assignment): List<List<Fact>> {
        return head.conjunctions.map { it.forAssignment(assignment, true, id, frontier) }
    }

    fun headForAssignment(assignment: Assignment): List<Fact> {
        return headDisjunctsForAssignment(assignment).flatten()
    }

    fun headChoiceForAssignment(assignment: Assignment): List<Fact> {
        if (disjunctIndex == null) {
            return headForAssignment(assignment)
        }

        return headDisjunctsForAssignment(assignment).get(disjunctIndex)
    }

    fun renameExistentialVariablesApart(): RuleWithId =
        Expressions.makeRule(
            Expressions.makeDisjunction(
                head.conjunctions.mapIndexed { disjunctIndex, conjunction ->
                    conjunction.literals.map { literal ->
                        Expressions.makePositiveLiteral(
                            literal.predicate,
                            literal.arguments.map { argument ->
                                if (argument is ExistentialVariable) {
                                    Expressions.makeExistentialVariable("${argument.name}_$disjunctIndex")
                                } else {
                                    argument
                                }
                            }
                        )
                    }
                }
            ),
            body
        ).toRuleWithId(id)

    fun getApplicableAssignments(allFacts: Map<Predicate, Set<Fact>>, newFactsSinceLastCheck: Map<Predicate, Set<Fact>>): Sequence<Assignment> {
        if (!isDisjunctiveExistential) {
            throw Exception("rules may only contain one conjunction in body")
        }

        // System.err.println(newFactsSinceLastCheck)

        val singleConjunction = body.conjunctions.first()
        val validAssignments = singleConjunction.assignmentsForFacts(allFacts, newFactsSinceLastCheck)

        // System.err.println("filter possible assignments")
        // System.err.println(possibleAssignments)
        // System.err.println(this)
        val assignmentsYieldingNewFacts: Sequence<Assignment> = validAssignments
            .filter { assignment ->
                headForAssignment(assignment).any { it !in allFacts.getOrDefault(it.predicate, setOf()) }
            }

        return assignmentsYieldingNewFacts
    }

    fun isBlockingItself(chaseVariant: ChaseVariant): Boolean {
        val assignment: Assignment = body.universalVariables
            .toList()
            .map {
                it to Expressions.makeAbstractConstant("BLOCK_ITSELF_$it")
            }
            .toMap()

        val bodyFacts = bodyForAssignment(assignment)
        val bodyPredicateMap = bodyFacts.predicateMap

        return isObsolete(this, assignment, chaseVariant, bodyPredicateMap)
    }

    val criticalRuleBody: Set<Fact>
        get() {
            val assignment: Assignment = variables
                .toList()
                .map {
                    it to Expressions.makeAbstractConstant("RULE_INSTANCE_$it")
                }
                .toMap()

            return bodyForAssignment(assignment).toHashSet()
        }

    val criticalRuleHead: Set<Fact>
        get() {
            val assignment: Assignment = variables
                .toList()
                .map {
                    it to Expressions.makeAbstractConstant("RULE_INSTANCE_$it")
                }
                .toMap()

            return if (isDeterministic) headForAssignment(assignment).toHashSet() else headChoiceForAssignment(assignment).toHashSet()
        }

    val criticalRuleInstance: Set<Fact>
        get() = criticalRuleBody + criticalRuleHead
}

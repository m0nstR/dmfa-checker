package dmfa.checker

/*
    DMFA Checker
    Trigger

    Copyright 2022 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.semanticweb.rulewerk.core.model.api.ExistentialVariable
import org.semanticweb.rulewerk.core.model.implementation.Expressions

typealias Trigger = Pair<RuleWithId, Assignment>

fun Trigger.isEqualTo(other: Trigger): Boolean = first == other.first && first.headChoiceForAssignment(second).toSet() == other.first.headChoiceForAssignment(other.second).toSet()

fun Trigger.yieldsSameResultAs(other: Trigger): Boolean = first.headChoiceForAssignment(second).toSet() == other.first.headChoiceForAssignment(other.second).toSet()

fun Trigger.isProducingSubsetOf(other: Trigger): Boolean = other.first.headChoiceForAssignment(other.second).toSet().containsAll(first.headChoiceForAssignment(second).toSet())

fun Trigger.isInterchangeableWith(other: Trigger): Boolean {
    val headDisjunctThis = if (first.disjunctIndex == null) Expressions.makeConjunction(first.head.conjunctions.flatten()) else first.head.conjunctions.get(first.disjunctIndex!!)
    val headDisjunctOther = if (other.first.disjunctIndex == null) Expressions.makeConjunction(other.first.head.conjunctions.flatten()) else other.first.head.conjunctions.get(other.first.disjunctIndex!!)

    val exisVarsThis: Set<ExistentialVariable> = headDisjunctThis.literals.flatMap { it.arguments }.filterIsInstance<ExistentialVariable>().toSet()
    val exisVarsOther: Set<ExistentialVariable> = headDisjunctOther.literals.flatMap { it.arguments }.filterIsInstance<ExistentialVariable>().toSet()

    if (exisVarsThis.size != exisVarsOther.size) {
        return false
    }

    val permutations = (1..exisVarsThis.size).fold<Int, List<List<ExistentialVariable>>>(listOf(listOf())) { acc, _ ->
        acc.flatMap { list ->
            exisVarsThis
                .filter { it !in list }
                .map { list + it }
        }
    }

    val dummyAssignmentThis: Assignment = second + exisVarsThis.map { it to Expressions.makeAbstractConstant("INTERCHANGEABLE_CHECK_$it") }.toMap()
    val dummyAssignmentsOther: List<Assignment> = permutations.map { permutation -> other.second + exisVarsOther.mapIndexed { index, variable -> variable to dummyAssignmentThis.get(permutation[index])!! }.toMap() }

    val resultThis = headDisjunctThis.forAssignment(dummyAssignmentThis, false).toSet()

    val interchangeable = dummyAssignmentsOther.any { otherAssignment ->
        resultThis == headDisjunctOther.forAssignment(otherAssignment, false).toSet()
    }

    return interchangeable
}

package dmfa.checker

/*
    DMFA Checker
    Conjunction

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.semanticweb.rulewerk.core.model.api.AbstractConstant
import org.semanticweb.rulewerk.core.model.api.Conjunction
import org.semanticweb.rulewerk.core.model.api.ExistentialVariable
import org.semanticweb.rulewerk.core.model.api.Fact
import org.semanticweb.rulewerk.core.model.api.Predicate
import org.semanticweb.rulewerk.core.model.api.UniversalVariable
import org.semanticweb.rulewerk.core.model.api.Variable
import org.semanticweb.rulewerk.core.model.implementation.Expressions
import kotlin.streams.asSequence
import kotlin.streams.toList

fun Conjunction<*>.positionsOfVariable(variable: Variable): Map<Predicate, Set<Int>> = fold(mapOf()) { acc, literal ->
    acc.merge(literal.arguments.mapIndexed { i, arg -> if (arg == variable) { Pair(literal.predicate, i) } else { null } }.filterNotNull())
}

fun Conjunction<*>.forAssignment(assignment: Assignment, introduceSkolemTerms: Boolean, ruleId: Int? = null, frontier: List<UniversalVariable>? = null): List<Fact> {
    return literals.map { literal ->
        Expressions.makeFact(
            literal.predicate,
            literal.arguments.map {
                if (introduceSkolemTerms && it is ExistentialVariable) {
                    if (ruleId == null || frontier == null) {
                        throw Exception("in Conjunction.forAssignment ruleId and frontier are required if introduceSkolemTerms is set to true and existentially quantified variables are present")
                    }
                    SkolemTerm(it.name, ruleId, frontier.map { assignment.get(it)!! })
                } else if (it is AbstractConstant) {
                    it
                } else {
                    assignment.get(it)!!
                }
            }
        )
    }
}

fun Conjunction<*>.variablesForPositions(positions: Map<Predicate, Set<Int>>): Set<UniversalVariable> =
    filter { positions.containsKey(it.predicate) }
        .flatMap {
            it.arguments
                .filterIndexed { i, _ -> positions.get(it.predicate)!!.contains(i) }
                .filterIsInstance<UniversalVariable>()
        }
        .toSet()

fun Conjunction<*>.variablesOnlyAtPositions(positions: Map<Predicate, Set<Int>>, startPositions: Map<Predicate, Set<Int>>? = null): Set<UniversalVariable> {
    val init = startPositions ?: positions

    return variablesForPositions(init).filter { univVar -> positionsOfVariable(univVar).subtract(positions).isEmpty() }.toSet()
}

fun Conjunction<*>.isGuardedForVars(variables: Set<UniversalVariable>): Boolean {
    val intersected = universalVariables.toList().intersect(variables)
    return literals.isEmpty() || literals.any { it.arguments.containsAll(intersected) }
}

val Conjunction<*>.predicatePositionsToVariables: HashMap<Triple<Predicate, Int, Int>, Variable>
    get() =
        literals.foldIndexed(
            hashMapOf(),
            { predicateIndex, acc, next ->
                next.arguments.asSequence().foldIndexed(
                    acc,
                    { index, newAcc, arg ->
                        if (arg !is Variable) {
                            throw Exception("only variables expected as arguments of literals")
                        }

                        newAcc.put(Triple(next.predicate, predicateIndex, index), arg)
                        newAcc
                    }
                )
            }
        )

private fun assignmentForFact(fact: Fact, predicateIndex: Int, cachedPredicatePositionsToVariables: HashMap<Triple<Predicate, Int, Int>, Variable>): Assignment? {
    return fact.arguments.foldIndexed(
        hashMapOf(),
        { index, assignment, arg ->
            val position = Triple(fact.predicate, predicateIndex, index)
            val variable = cachedPredicatePositionsToVariables.get(position)!!

            if (assignment.containsKey(variable) && assignment.get(variable)!! != arg) {
                return null
            }

            assignment.put(variable, arg)
            assignment
        }
    )
}

fun Conjunction<*>.assignmentsForFacts(allFacts: Map<Predicate, Set<Fact>>, atLeastOneFactOf: Map<Predicate, Set<Fact>>, startAssignment: Assignment? = null): Sequence<Assignment> {
    val predicateSequence = literals.map { it.predicate }.asSequence()

    val cachedPredicatePositionsToVariables = predicatePositionsToVariables

    val possibleAssignments: Sequence<Assignment> = predicateSequence.withIndex().flatMap { (startPredicateIndex, startPredicate) ->
        // extract this, since it's also used within fold further down
        val factsForStartPredicate: Sequence<Fact> = atLeastOneFactOf.getOrDefault(startPredicate, setOf()).asSequence()

        if (factsForStartPredicate.none()) {
            // return from outmost function (there are no assignments at all)
            return@flatMap sequenceOf<Assignment>()
        }

        val assignmentsForStartPredicateUnfiltered: Sequence<Assignment> = factsForStartPredicate
            .map {
                assignmentForFact(it, startPredicateIndex, cachedPredicatePositionsToVariables)
            }
            .filterNotNull()

        val assignmentsForStartPredicate = if (startAssignment != null) {
            assignmentsForStartPredicateUnfiltered
                .filter { newAssignment ->
                    val overlappingKeys = newAssignment.keys.filter { it in startAssignment.keys }

                    overlappingKeys.all { newAssignment.get(it) == startAssignment.get(it) }
                }
                .map { newAssignment ->
                    startAssignment + newAssignment
                }
        } else {
            assignmentsForStartPredicateUnfiltered
        }

        if (assignmentsForStartPredicate.none()) {
            // return from outmost function (there are no assignments at all)
            return@flatMap sequenceOf<Assignment>()
        }

        predicateSequence.foldIndexed(
            assignmentsForStartPredicate,
            { predicateIndex, assignments, predicate ->
                if (predicateIndex == startPredicateIndex) {
                    assignments
                } else {
                    val factsForPredicate: Sequence<Fact> = allFacts.getOrDefault(predicate, setOf())
                        .asSequence()
                        .filter {
                            startPredicateIndex < predicateIndex || it !in atLeastOneFactOf.getOrDefault(predicate, setOf())
                        }

                    if (factsForPredicate.none()) {
                        // return from outmost function (there are no assignments at all)
                        return@flatMap sequenceOf<Assignment>()
                    }

                    val assignmentsForPredicate: Sequence<Assignment> = factsForPredicate
                        .map {
                            assignmentForFact(it, predicateIndex, cachedPredicatePositionsToVariables)
                        }
                        .filterNotNull()

                    assignments.flatMap { assignment ->
                        assignmentsForPredicate
                            .filter { newAssignment ->
                                val overlappingKeys = newAssignment.keys.filter { it in assignment.keys }

                                overlappingKeys.all { newAssignment.get(it) == assignment.get(it) }
                            }
                            .map { newAssignment ->
                                assignment + newAssignment
                            }
                    }
                }
            }
        )
    }

    // System.err.println("filter possible assignments")
    // System.err.println(possibleAssignments)
    // System.err.println(this)
    val validAssignments: Sequence<Assignment> = possibleAssignments
        .filter { assignment ->
            // false indicated that we treat exitentially quantified variables as universally quantified by not introducing skolem terms
            forAssignment(assignment, false).all { it in allFacts.getOrDefault(it.predicate, setOf()) }
        }

    return validAssignments
}

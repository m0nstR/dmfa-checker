package dmfa.checker

/*
    DMFA Checker
    Graph

    Copyright 2023 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

class Graph<S, T> {
    // for each vertex (label), we list the the vertices reachable in one step together with the edge label
    private val adjacencyMap = mutableMapOf<S, MutableList<Pair<S, T>>>()

    fun addVertex(vertex: Pair<S, List<Pair<S, T>>>) {
        val value = adjacencyMap.get(vertex.first)
        if (value == null) {
            adjacencyMap.put(vertex.first, vertex.second.toMutableList())
        } else {
            value.addAll(vertex.second)
        }
    }

    fun addEdge(source: S, target: S, label: T) {
        val value = adjacencyMap.get(source)
        if (value == null) {
            adjacencyMap.put(source, mutableListOf(Pair(target, label)))
        } else {
            value.add(Pair(target, label))
        }
    }

    private fun reachableTrackingEdgeType(from: Map<S, Boolean>, edgeType: T): Map<S, Boolean> {
        // boolean indicates if special edge can be involved in reaching the vertex
        val reachable: MutableMap<S, Boolean> = from.toMutableMap()
        var changedInLastIteration = true

        while (changedInLastIteration) {
            changedInLastIteration = false

            reachable.keys.flatMap { adjacencyMap.get(it) ?: listOf() }.forEach { (s, t) ->
                val value = reachable.get(s)
                val tIsEdgeType = t == edgeType

                if (value == null || (!value && tIsEdgeType)) {
                    reachable.put(s, tIsEdgeType)
                    changedInLastIteration = true
                }
            }
        }

        return reachable
    }

    private fun reachableInvolvingEdgeType(from: S, edgeType: T): Set<S> = reachableTrackingEdgeType(mutableMapOf(Pair(from, false)), edgeType).filterValues { it }.keys

    fun verticesWithCycleForEdgeType(edgeType: T): Sequence<S> = adjacencyMap.keys.asSequence().filter { reachableInvolvingEdgeType(it, edgeType).contains(it) }

    fun hasCycleInvolvingEdgeType(edgeType: T): Boolean = verticesWithCycleForEdgeType(edgeType).any()

    fun verticesWithInfiniteRankWithRespectToEdgeType(edgeType: T): Set<S> {
        val verticesWithCycleForEdgeType = verticesWithCycleForEdgeType(edgeType)

        // we have to put an edge type here but we ignore the indicator in the result; we are just abusing the private method here to avoid reimplementing
        return reachableTrackingEdgeType(verticesWithCycleForEdgeType.map { Pair(it, false) }.toMap(), edgeType).keys
    }
}

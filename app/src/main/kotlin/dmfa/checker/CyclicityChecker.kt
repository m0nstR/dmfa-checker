package dmfa.checker

/*
    DMFA Checker
    Cyclicity Checker

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.semanticweb.rulewerk.core.model.api.Fact
import org.semanticweb.rulewerk.core.model.api.Rule
import kotlin.math.min

fun checkCyclicity(rawRules: List<Rule>, chaseVariant: ChaseVariant, respectDisjunctions: Boolean, unblockabilityUniqueConstants: Boolean, unblockabilityApplicabilityCondition: UnblockabilityApplicabilityCondition, unblockabilityIncludeRuleBodiesWhenBacktracking: Boolean, unblockabilityDisjToConj: Boolean, unblockabilityIgnoreRedundantDerivations: Boolean, tooManyFactBound: Int? = null, ruleId: Int? = null): Boolean {
    val initialPredicates = rawRules.flatMap {
        (
            it.head.conjunctions.flatMap { it.literals.map { it.predicate } } +
                it.body.conjunctions.flatMap { it.literals.map { it.predicate } }
            )
    }.toHashSet()

    var abortDueToTooManyFacts = false

    // NOTE: index of rule in this list equals it's id. That makes retrieval easy. (Used in blocked check.)
    val indexedRules: List<RuleWithId> = rawRules
        // we give each rule an id (just and index)
        .mapIndexed { index, rule -> rule.toRuleWithId(index) }
        // we have to make sure that the same existential variable does not occur in multiple disjuncts
        // so we rename them appart (needed for blocked check)
        .map { it.renameExistentialVariablesApart() }

    // ATTENTION: this differs from the description in the paper; we only take a small portion of variants
    // 2. Iterate over deterministic head-choices
    val ruleSetVariants: Sequence<List<RuleWithId>> = if (respectDisjunctions) {
        val maxHeadSize = indexedRules.maxOf { it.head.conjunctions.size }

        (1..maxHeadSize).asSequence().map { disjunctIndex -> indexedRules.map { rule -> RuleWithId(rule.rule, rule.id, min(rule.head.conjunctions.size, disjunctIndex) - 1) } }
        // indexedRules
        // .fold(sequenceOf(listOf())) { acc, rule ->
        // acc.flatMap { ruleSet -> (1..rule.head.conjunctions.size).asSequence().map { ruleSet + RuleWithId(rule.rule, rule.id, it - 1) } }
        // }
    } else {
        // for RMFC we "ignore" disjunctive rules when reasoning so we do not need to consider deterministic head-choices
        sequenceOf(indexedRules)
    }

    var variantCounter: Int = 1

    for (ruleSet in ruleSetVariants) {
        DebugLogger.log("Variant count: $variantCounter")
        variantCounter += 1

        for (ruleToCheck in ruleSet) {
            // Check for single Rule
            DebugLogger.log("rule ${ruleToCheck.id} of ${ruleSet.size}")

            if (ruleId != null && ruleToCheck.id != ruleId) {
                continue
            }

            // 0. Skip rules that are non-generating or self-blocking
            if (!ruleToCheck.isGenerating || ruleToCheck.isBlockingItself(chaseVariant)) {
                continue
            }

            if (!respectDisjunctions && !ruleToCheck.isDeterministic) {
                continue
            }

            //
            // 1. compute critical rule instance

            val initialFacts = ruleToCheck.criticalRuleInstance

            // compute overapproximation set (e.g. DMFC(R, \rho, \beta))
            // VERY CLOSE TO ACYCLICITY CHECK CONCEPTUALLY

            val datalogPredicates = ruleSet.filter { it.isDatalog }.flatMap {
                (
                    it.head.conjunctions.flatMap { it.literals.map { it.predicate } } +
                        it.body.conjunctions.flatMap { it.literals.map { it.predicate } }
                    )
            }.toHashSet()

            val nonDatalogPredicates = ruleSet.filter { !it.isDatalog }.flatMap {
                (
                    it.head.conjunctions.flatMap { it.literals.map { it.predicate } } +
                        it.body.conjunctions.flatMap { it.literals.map { it.predicate } }
                    )
            }.toHashSet()

            val predicatesInDatalogAndExistentialRules = datalogPredicates.intersect(nonDatalogPredicates)

            val datalogReasoner = VlogDatalogReasoner(ruleSet.filter { it.isDatalog }, predicatesInDatalogAndExistentialRules)

            // val functionalNamesFromTargetRule = initialFacts.flatMap { it.arguments }.map { if (it is SkolemTerm) it.functionName else null }.filterNotNull().toHashSet()
            val existentialReasoner = ExistentialReasoner(
                initialPredicates,
                ruleSet.filter { if (respectDisjunctions) !it.isDatalog else (it.isDeterministic && !it.isDatalog) },
                true
            ) { rule, assignment, factsPerPredicate ->
                assignment.values.all { it !is SkolemTerm || !it.isCyclic() } &&
                    // for (D)RMFC only functional terms with a symbol from the critical rule instance can occur
                    // for DMFC we artificially restrict the use of disjucntive rules such that they only use such functional terms
                    // when (chaseVariant) {
                    // ChaseVariant.SKOLEM -> rule.isDeterministic || rule.frontier.map { assignment.get(it)!! }.all { it !is SkolemTerm || it.containsFunctionSymbolFromList(functionalNamesFromTargetRule) }
                    // ChaseVariant.RESTRICTED -> rule.frontier.map { assignment.get(it)!! }.any { it is SkolemTerm }
                    // } &&
                    // !!! DMFC idea seems to not be correct technically !!!
                    // we use restricted variant always now
                    rule.frontier.map { assignment.get(it)!! }.any { it is SkolemTerm } &&
                    (
                        (chaseVariant == ChaseVariant.SKOLEM && rule.isDeterministic) ||
                            isUnblockable(rule, assignment, ruleSet, chaseVariant, factsPerPredicate, initialFacts, unblockabilityUniqueConstants, unblockabilityApplicabilityCondition, unblockabilityIncludeRuleBodiesWhenBacktracking, unblockabilityDisjToConj, unblockabilityIgnoreRedundantDerivations)
                        ) &&
                    // if ruleToCheck equals rule, then the substitution needs to be injective i.e. all terms in the range need to be different
                    (ruleToCheck.id != rule.id || assignment.values.distinct().size == assignment.values.size)
            }
            //
            // keeps track of new non-datalog facts from last iteration
            var newFactsSinceLastApplicabilityCheck = mutableSetOf<Fact>()
            newFactsSinceLastApplicabilityCheck.addAll(initialFacts)

            do {
                // 2.1. run chase on datalog rules

                DebugLogger.log("datalog closure main loop")

                val datalogClosureResult = datalogReasoner.computeNewDatalogFacts(newFactsSinceLastApplicabilityCheck)

                newFactsSinceLastApplicabilityCheck.addAll(datalogClosureResult)
                DebugLogger.log("number of new facts after datalog closure: ${newFactsSinceLastApplicabilityCheck.size}")
                // DebugLogger.log("$newFactsSinceLastApplicabilityCheck")

                // 2.2. chase existential rules (also disjunctive)

                DebugLogger.log("chase existential rules")

                val newNonDatalogFactsInThisIteration = existentialReasoner.computeNewFacts(newFactsSinceLastApplicabilityCheck)

                // 2.3. check for cyclic terms
                DebugLogger.log("check cyclicity for chase result")
                if (
                    newNonDatalogFactsInThisIteration
                        .flatMap { it.arguments }
                        .any { term ->
                            term is SkolemTerm &&
                                term.ruleId == ruleToCheck.id &&
                                term.isCyclicForRule(ruleToCheck)
                        }
                ) {
                    DebugLogger.log("${newNonDatalogFactsInThisIteration.flatMap{it.arguments}.filter{it is SkolemTerm && it.isCyclicForRule(ruleToCheck)}}")
                    return true
                }

                DebugLogger.log("number of new facts: ${newNonDatalogFactsInThisIteration.size}")
                // DebugLogger.log("$newNonDatalogFactsInThisIteration")

                if (tooManyFactBound != null && newNonDatalogFactsInThisIteration.size > tooManyFactBound) {
                    abortDueToTooManyFacts = true
                    break
                }

                // copy list
                newFactsSinceLastApplicabilityCheck = newNonDatalogFactsInThisIteration.toMutableSet()
            } while (!newNonDatalogFactsInThisIteration.isEmpty())
        }
    }

    if (abortDueToTooManyFacts) {
        throw Exception("too many facts produced")
    } else if (ruleId != null) {
        throw Exception("check with specific rule was not cyclic; this is not sound")
    } else {
        return false
    }
}

@file:OptIn(kotlinx.cli.ExperimentalCli::class)

package dmfa.checker

/*
    DMFA Checker
    App

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.Subcommand
import kotlinx.cli.default
import kotlinx.cli.required
import org.semanticweb.rulewerk.core.model.api.Rule
import org.semanticweb.rulewerk.core.reasoner.KnowledgeBase
import org.semanticweb.rulewerk.parser.RuleParser
import org.semanticweb.rulewerk.reasoner.vlog.VLogReasoner

enum class TypeOfCheck {
    TERMINATION,
    NON_TERMINATION
}

fun main(args: Array<String>) {
    // parse command line options
    val parser = ArgParser("ruleset-classifier")

    val noNewline by parser.option(ArgType.Boolean, shortName = "n", description = "omit newline character after output").default(false)
    val debug by parser.option(ArgType.Boolean, shortName = "d", description = "enable debug logging").default(false)

    fun executeCheck(check: (List<Rule>) -> Boolean) {
        DebugLogger.isEnabled = debug

        val kb = RuleParser.parse(System.`in`)
        val rules = kb.rules

        val result = check(rules)

        if (noNewline) {
            print("$result")
        } else {
            println("$result")
        }
    }

    class Joinless : Subcommand("joinless", "Check if rule set if joinless") {
        override fun execute() {
            executeCheck { it.isJoinless }
        }
    }

    class Linear : Subcommand("linear", "Check if rule set if linear") {
        override fun execute() {
            executeCheck { it.isLinear }
        }
    }

    class Guarded : Subcommand("guarded", "Check if rule set is guarded") {
        override fun execute() {
            executeCheck { it.isGuarded }
        }
    }

    class Sticky : Subcommand("sticky", "Check if rule set is sticky") {
        override fun execute() {
            executeCheck { it.isSticky }
        }
    }

    class DomainRestricted : Subcommand("domain-restricted", "Check if rule set is domain-restricted") {
        override fun execute() {
            executeCheck { it.isDomainRestricted }
        }
    }

    class FrontierOne : Subcommand("frontier-one", "Check if rule set is frontier-one") {
        override fun execute() {
            executeCheck { it.isFrontierOne }
        }
    }

    class Datalog : Subcommand("datalog", "Check if rule set is datalog") {
        override fun execute() {
            executeCheck { it.isDatalog }
        }
    }

    class Monadic : Subcommand("monadic", "Check if rule set is monadic") {
        override fun execute() {
            executeCheck { it.isMonadic }
        }
    }

    class FrontierGuarded : Subcommand("frontier-guarded", "Check if rule set is frontier-guarded") {
        override fun execute() {
            executeCheck { it.isFrontierGuarded }
        }
    }

    class WeaklyGuarded : Subcommand("weakly-guarded", "Check if rule set is weakly-guarded") {
        override fun execute() {
            executeCheck { it.isWeaklyGuarded }
        }
    }

    class WeaklyFrontierGuarded : Subcommand("weakly-frontier-guarded", "Check if rule set is weakly-frontier-guarded") {
        override fun execute() {
            executeCheck { it.isWeaklyFrontierGuarded }
        }
    }

    class Warded : Subcommand("warded", "Check if rule set is warded") {
        override fun execute() {
            executeCheck { it.isWarded }
        }
    }

    class JointlyGuarded : Subcommand("jointly-guarded", "Check if rule set is jointly-guarded") {
        override fun execute() {
            executeCheck { it.toIndexed.isJointlyGuarded }
        }
    }

    class JointlyFrontierGuarded : Subcommand("jointly-frontier-guarded", "Check if rule set is jointly-frontier-guarded") {
        override fun execute() {
            executeCheck { it.toIndexed.isJointlyFrontierGuarded }
        }
    }

    class WeaklyAcyclic : Subcommand("weakly-acyclic", "Check if rule set is weakly-acyclic") {
        override fun execute() {
            executeCheck { it.isWeaklyAcyclic }
        }
    }

    class JointlyAcyclic : Subcommand("jointly-acyclic", "Check if rule set is jointly-acyclic") {
        override fun execute() {
            executeCheck { it.toIndexed.isJointlyAcyclic }
        }
    }

    class WeaklySticky : Subcommand("weakly-sticky", "Check if rule set is weakly-sticky") {
        override fun execute() {
            executeCheck { it.isWeaklySticky }
        }
    }

    class GlutGuarded : Subcommand("glut-guarded", "Check if rule set is glut-guarded") {
        override fun execute() {
            executeCheck { it.toIndexed.isGlutGuarded }
        }
    }

    class GlutFrontierGuarded : Subcommand("glut-frontier-guarded", "Check if rule set is glut-frontier-guarded") {
        override fun execute() {
            executeCheck { it.toIndexed.isGlutFrontierGuarded }
        }
    }

    class Shy : Subcommand("shy", "Check if rule set is shy") {
        override fun execute() {
            executeCheck { it.toIndexed.isShy }
        }
    }

    class Mfa : Subcommand("mfa", "Check if rule set is MFA (using Vlog implementation)") {
        override fun execute() {
            executeCheck {
                val rulesForCheck = it.map { it.disjunctionsToConjunctions() }

                val vlogKb = KnowledgeBase()
                vlogKb.addStatements(rulesForCheck)

                val vlogReasoner = VLogReasoner(vlogKb)
                val vlogResult = vlogReasoner.isMFA()

                vlogResult
            }
        }
    }

    class Dmfa : Subcommand("dmfa", "Check if rule set is DMFA") {
        val targetNestingDepth by option(ArgType.Int, shortName = "depth", description = "minimal nesting depth for cyclic terms that the check looks for; 1 means one cycle like f(f(a))").default(1)

        override fun execute() {
            executeCheck {
                checkAcyclicity(it, ChaseVariant.SKOLEM, targetNestingDepth, false)
            }
        }
    }

    class Rmfa : Subcommand("rmfa", "Check if rule set is RMFA") {
        val targetNestingDepth by option(ArgType.Int, shortName = "depth", description = "minimal nesting depth for cyclic terms that the check looks for; 1 means one cycle like f(f(a))").default(1)

        override fun execute() {
            executeCheck {
                checkAcyclicity(it, ChaseVariant.RESTRICTED, targetNestingDepth, false)
            }
        }
    }

    class Mfc : Subcommand("mfc", "Check if rule set is MFC (using Vlog implementation)") {
        override fun execute() {
            executeCheck {
                val vlogKb = KnowledgeBase()
                vlogKb.addStatements(it.filter { it.isDeterministic })

                val vlogReasoner = VLogReasoner(vlogKb)

                // dirty workaround to load KB because load cannot be called directly...
                val mfaResult = vlogReasoner.isMFA()

                if (mfaResult) {
                    false
                } else {
                    vlogReasoner.isMFC()
                }
            }
        }
    }

    class Dmfc : Subcommand("dmfc", "Check if rule set is DMFC") {
        override fun execute() {
            executeCheck {
                checkCyclicity(it, ChaseVariant.SKOLEM, true, false, UnblockabilityApplicabilityCondition.EQUIVALENCE_ONLY_RESULT, false, false, false)
            }
        }
    }

    class Drpc : Subcommand("drpc", "Check if rule set is DRPC") {
        override fun execute() {
            executeCheck {
                checkCyclicity(it, ChaseVariant.RESTRICTED, false, false, UnblockabilityApplicabilityCondition.EQUIVALENCE_INCLUDING_RULE, false, false, false)
            }
        }
    }

    class Rpc : Subcommand("rpc", "Check if rule set is RPC") {
        override fun execute() {
            executeCheck {
                checkCyclicity(it, ChaseVariant.RESTRICTED, true, true, UnblockabilityApplicabilityCondition.EQUIVALENCE_ONLY_RESULT, false, false, true)
            }
        }
    }

    class ChaseLikeCheck : Subcommand("generic-chaselike", "MEANT FOR DEVELOPMENT PURPOSES ONLY! Generic Chase-Like Termination or Non-Termination check with many feature flags that can be (almost) freely combined.") {
        val typeOfCheck by option(
            ArgType.Choice<TypeOfCheck>(),
            shortName = "t",
            description = "Which check to perform"
        ).required()
        val chaseVariant by option(
            ArgType.Choice<ChaseVariant>(),
            shortName = "cv",
            description = "Chase Variant to use for semantic (non-)termination check (Skolem or Restricted)"
        ).required()
        val respectDisjunctions by option(ArgType.Boolean, shortName = "disj", description = "use the check version that respects rules with disjunctions (omitting this may cause unsound results in certain settings)").default(false)
        val replaceDisjunctionsWithConjunctions by option(ArgType.Boolean, shortName = "disjToConj", description = "replace all disjunctions with conjunctions before the check (this may cause unsound results in certain settings)").default(false)
        val targetNestingDepth by option(ArgType.Int, shortName = "depth", description = "minimal nesting depth for cyclic terms that the check looks for; 1 means one cycle like f(f(a)) (only for certain termination settings)").default(1)
        val unblockabilityUniqueConstants by option(ArgType.Boolean, shortName = "uc", description = "replace existential variables by unique constants instead of star (only has an effect for certain non-termination settings)").default(false)

        val unblockabilityApplicabilityCondition by option(
            ArgType.Choice<UnblockabilityApplicabilityCondition>(),
            shortName = "uac",
            description = "Applicability condition to use for triggers within the unblockable check"
        ).default(UnblockabilityApplicabilityCondition.EQUIVALENCE_ONLY_RESULT)

        val unblockabilityIncludeRuleBodiesWhenBacktracking by option(ArgType.Boolean, shortName = "ublkInclBodies", description = "for backtrack assignment in unblockability check also use rule bodies (not only heads)").default(false)
        val unblockabilityDisjToConj by option(ArgType.Boolean, shortName = "ublkDisjToConj", description = "in unblockability replace disjunctions by conjunctions in rules").default(false)

        val unblockabilityIgnoreRedundantDerivations by option(ArgType.Boolean, shortName = "ublkIgnoreRedundant", description = "do not derive star or unique constant version of facts that are already in backtracking").default(false)

        val blockednessNoDatalogClosure by option(ArgType.Boolean, shortName = "blkNoDlgCl", description = "don't close under datalog rules in blocked check (only has an effect for certain termination settings)").default(false)

        // Misc Options
        val tooManyFactBound by option(ArgType.Int, shortName = "tmfb", description = "upper bound for facts before abort (only for certain NON_TERMINATION checks; has no effect otherwise)")
        val ruleId by option(ArgType.Int, shortName = "ri", description = "only check for rule with this id (only for certain NON_TERMINATION checks; has no effect otherwise)")

        override fun execute() {
            DebugLogger.isEnabled = debug

            // do work
            val kb = RuleParser.parse(System.`in`)
            val rulesForCheck = if (replaceDisjunctionsWithConjunctions) kb.rules.map { it.disjunctionsToConjunctions() } else kb.rules

            val result = when (typeOfCheck) {
                TypeOfCheck.TERMINATION -> {
                    if (respectDisjunctions) {
                        checkAcyclicity(rulesForCheck, chaseVariant, targetNestingDepth, blockednessNoDatalogClosure)
                    } else {
                        val vlogKb = KnowledgeBase()
                        vlogKb.addStatements(rulesForCheck.filter { it.isDeterministic })

                        val vlogReasoner = VLogReasoner(vlogKb)
                        val vlogResult = when (chaseVariant) {
                            ChaseVariant.SKOLEM -> vlogReasoner.isMFA()
                            ChaseVariant.RESTRICTED -> vlogReasoner.isRMFA()
                        }

                        vlogResult
                    }
                }
                TypeOfCheck.NON_TERMINATION -> {
                    if (chaseVariant == ChaseVariant.SKOLEM && !respectDisjunctions) {
                        val vlogKb = KnowledgeBase()
                        vlogKb.addStatements(rulesForCheck.filter { it.isDeterministic })

                        val vlogReasoner = VLogReasoner(vlogKb)
                        // dirty workaround to load KB because load cannot be called directly...
                        vlogReasoner.isMFA()
                        val vlogResult = vlogReasoner.isMFC()

                        vlogResult
                    } else {
                        checkCyclicity(rulesForCheck, chaseVariant, respectDisjunctions, unblockabilityUniqueConstants, unblockabilityApplicabilityCondition, unblockabilityIncludeRuleBodiesWhenBacktracking, unblockabilityDisjToConj, unblockabilityIgnoreRedundantDerivations, tooManyFactBound, ruleId)
                    }
                }
            }

            System.err.println("$typeOfCheck, $chaseVariant, respectDisjunctions $respectDisjunctions, replaceDisjunctionsWithConjunctions $replaceDisjunctionsWithConjunctions, unblockabilityUniqueConstants $unblockabilityUniqueConstants, unblockabilityApplicabilityCondition $unblockabilityApplicabilityCondition, unblockabilityIncludeRuleBodiesWhenBacktracking $unblockabilityIncludeRuleBodiesWhenBacktracking, unblockabilityDisjToConj $unblockabilityDisjToConj, blockednessNoDatalogClosure $blockednessNoDatalogClosure, targetNestingDepth $targetNestingDepth, ruleId $ruleId: $result")

            if (noNewline) {
                print("$result")
            } else {
                println("$result")
            }
        }
    }

    parser.subcommands(
        Joinless(),
        Linear(),
        Guarded(),
        Sticky(),
        DomainRestricted(),
        FrontierOne(),
        Datalog(),
        Monadic(),
        FrontierGuarded(),
        WeaklyGuarded(),
        WeaklyFrontierGuarded(),
        Warded(),
        JointlyGuarded(),
        JointlyFrontierGuarded(),
        WeaklyAcyclic(),
        JointlyAcyclic(),
        WeaklySticky(),
        GlutGuarded(),
        GlutFrontierGuarded(),
        Shy(),
        Mfa(),
        Dmfa(),
        Rmfa(),
        Mfc(),
        Dmfc(),
        Drpc(),
        Rpc(),
        ChaseLikeCheck()
    )
    parser.parse(args)
}

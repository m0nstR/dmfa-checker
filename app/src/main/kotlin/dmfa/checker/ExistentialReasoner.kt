package dmfa.checker

/*
    DMFA Checker
    Existential Reasoner

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.semanticweb.rulewerk.core.model.api.Fact
import org.semanticweb.rulewerk.core.model.api.Predicate

class ExistentialReasoner(predicates: Set<Predicate>, val rulesForReasoning: List<RuleWithId>, val paralleliseRules: Boolean, initialFacts: Set<Fact>? = null, val filterAssignmentsPredicate: ((rule: RuleWithId, assignment: Assignment, factsPerPredicate: Map<Predicate, HashSet<Fact>>) -> Boolean)? = null) {
    val factsPerPredicate: Map<Predicate, HashSet<Fact>>

    init {
        factsPerPredicate = predicates.map {
            it to hashSetOf<Fact>()
        }.toMap()

        if (initialFacts != null) {
            val initialFactsPerPredicate = initialFacts.predicateMap

            initialFactsPerPredicate.forEach { k, v ->
                if (factsPerPredicate.containsKey(k)) {
                    factsPerPredicate.get(k)!!.addAll(v)
                }
            }
        }

        // DebugLogger.log("rulesForReasoning: ${rulesForReasoning.size}")
    }

    private fun computeNewFactsForRule(rule: RuleWithId, newFactsPerPredicate: Map<Predicate, Set<Fact>>): Set<Fact> {
        val assignments = rule.getApplicableAssignments(factsPerPredicate, newFactsPerPredicate)

        val allowedAssignments = if (filterAssignmentsPredicate != null) {
            assignments.filter { assignment -> filterAssignmentsPredicate.invoke(rule, assignment, factsPerPredicate) }
        } else {
            assignments
        }

        return allowedAssignments
            .flatMap { rule.headChoiceForAssignment(it) }
            .filter { it !in factsPerPredicate.get(it.predicate)!! }
            .toSet()
    }

    fun computeNewFacts(newFactsInIteration: Set<Fact>): Set<Fact> {
        val newFactsPerPredicate = newFactsInIteration.predicateMap
        // System.err.println("prediacteMap finished")

        newFactsPerPredicate.forEach { k, v ->
            if (factsPerPredicate.containsKey(k)) {
                factsPerPredicate.get(k)!!.addAll(v)
            }
        }

        // System.err.println("start")
        if (paralleliseRules) {
            return runBlocking {
                rulesForReasoning
                    .map { rule ->
                        async(Dispatchers.Default) {
                            computeNewFactsForRule(rule, newFactsPerPredicate)
                        }
                    }
                    .flatMap { it.await() }
                    .toSet()
            }
        } else {
            return rulesForReasoning
                .flatMap { rule -> computeNewFactsForRule(rule, newFactsPerPredicate) }
                .toSet()
        }
    }
}

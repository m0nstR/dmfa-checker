package dmfa.checker

/*
    DMFA Checker
    Skolem Term

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.semanticweb.rulewerk.core.model.api.AbstractConstant
import org.semanticweb.rulewerk.core.model.api.Term
import org.semanticweb.rulewerk.core.model.implementation.AbstractConstantImpl
import org.semanticweb.rulewerk.core.model.implementation.Expressions
import kotlin.streams.asSequence

fun Term.withUniqueConstants(prefix: String?): AbstractConstant =
    if (this is SkolemTerm) {
        SkolemTerm(
            variableName,
            ruleId,
            arguments.mapIndexed { index, argument ->
                argument.withUniqueConstants("$prefix$index")
            }
        )
    } else if (this is AbstractConstant) {
        Expressions.makeAbstractConstant("UNIQUE_${prefix}_$name")
    } else {
        throw Exception("only Skolem Term and Abstract Constant allowed")
    }

class SkolemTerm(val variableName: String, val ruleId: Int, val arguments: List<Term>) : AbstractConstantImpl("$ruleId$variableName(${arguments.map { it.name }.joinToString(",")})") {
    private fun searchFunctionalName(names: Map<String, Int>, targetDepth: Int = 1, rule: RuleWithId? = null): Boolean {
        if (functionalArguments.map { it.functionName }.any { names.getOrDefault(it, 0) >= targetDepth }) {
            return true
        }

        return functionalArguments
            .any {
                it.searchFunctionalName(
                    names + (if (rule == null || it.ruleId == rule.id) mapOf(it.functionName to (names.getOrDefault(it.functionName, 0) + 1)) else mapOf()),
                    targetDepth,
                    rule
                )
            }
    }

    fun containsFunctionSymbolFromList(names: Set<String>): Boolean {
        if (functionName in names) {
            return true
        }

        return functionalArguments.any { it.containsFunctionSymbolFromList(names) }
    }

    fun isCyclic(targetDepth: Int = 1): Boolean = searchFunctionalName(mapOf(functionName to 1), targetDepth)

    fun isCyclicForRule(rule: RuleWithId): Boolean = searchFunctionalName(
        (if (ruleId == rule.id) mapOf(functionName to 1) else mapOf()),
        1,
        rule
    )

    val functionName: String
        get() = "$ruleId$variableName"

    val functionalArguments: Sequence<SkolemTerm>
        get() = arguments.asSequence().filterIsInstance<SkolemTerm>()

    val subterms: Sequence<SkolemTerm>
        get() = sequenceOf(this) + functionalArguments.flatMap { it.subterms }
}

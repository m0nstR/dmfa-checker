package dmfa.checker

/*
    DMFA Checker
    Acyclicity Checker

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.semanticweb.rulewerk.core.model.api.Fact
import org.semanticweb.rulewerk.core.model.api.Rule

fun checkAcyclicity(rawRules: List<Rule>, chaseVariant: ChaseVariant, targetCyclicTermDepth: Int, blockednessNoDatalogClosure: Boolean): Boolean {
    val initialPredicates = rawRules.flatMap {
        (
            it.head.conjunctions.flatMap { it.literals.map { it.predicate } } +
                it.body.conjunctions.flatMap { it.literals.map { it.predicate } }
            )
    }.toHashSet()

    // NOTE: index of rule in this list equals it's id. That makes retrieval easy. (Used in blocked check.)
    val indexedRules: List<RuleWithId> = rawRules
        // we give each rule an id (just and index)
        .mapIndexed { index, rule -> rule.toRuleWithId(index) }
        // we have to make sure that the same existential variable does not occur in multiple disjuncts
        // so we rename them appart (needed for blocked check)
        .map { it.renameExistentialVariablesApart() }

    val datalogPredicates = indexedRules.filter { it.isDatalog }.flatMap {
        (
            it.head.conjunctions.flatMap { it.literals.map { it.predicate } } +
                it.body.conjunctions.flatMap { it.literals.map { it.predicate } }
            )
    }.toHashSet()

    val nonDatalogPredicates = indexedRules.filter { !it.isDatalog }.flatMap {
        (
            it.head.conjunctions.flatMap { it.literals.map { it.predicate } } +
                it.body.conjunctions.flatMap { it.literals.map { it.predicate } }
            )
    }.toHashSet()

    val predicatesInDatalogAndExistentialRules = datalogPredicates.intersect(nonDatalogPredicates)

    val datalogReasoner = VlogDatalogReasoner(indexedRules.filter { it.isDatalog }, predicatesInDatalogAndExistentialRules)
    // val datalogReasoner = VlogDatalogReasoner(indexedRules.filter { it.isDatalog })
    // val datalogReasoner = EnrichedDatalogReasoner(indexedRules)

    val existentialReasoner = ExistentialReasoner(initialPredicates, indexedRules.filter { !it.isDatalog }, true) { rule, assignment, factsPerPredicate ->
        (chaseVariant == ChaseVariant.SKOLEM && rule.isDeterministic) || !isBlocked(rule, assignment, indexedRules, chaseVariant, factsPerPredicate, blockednessNoDatalogClosure)
    }

    // 1. compute critical instance
    val initialFacts = criticalInstance(initialPredicates)

    // keeps track of new non-datalog facts from last iteration
    var newFactsSinceLastApplicabilityCheck = mutableSetOf<Fact>()
    newFactsSinceLastApplicabilityCheck.addAll(initialFacts)

    do {
        // 2.1. run chase on datalog rules

        DebugLogger.log("datalog closure main loop")

        val datalogClosureResult = datalogReasoner.computeNewDatalogFacts(newFactsSinceLastApplicabilityCheck)

        newFactsSinceLastApplicabilityCheck.addAll(datalogClosureResult)
        DebugLogger.log("number of new facts after datalog closure: ${newFactsSinceLastApplicabilityCheck.size}")

        // 2.2. chase existential rules (also disjunctive)

        DebugLogger.log("chase existential rules")

        val newNonDatalogFactsInThisIteration = existentialReasoner.computeNewFacts(newFactsSinceLastApplicabilityCheck)

        // 2.3. check for cyclic terms
        DebugLogger.log("check cyclicity for chase result")
        if (newNonDatalogFactsInThisIteration.flatMap { it.arguments }.any { it is SkolemTerm && it.isCyclic(targetCyclicTermDepth) }) {
            DebugLogger.log("${newNonDatalogFactsInThisIteration.flatMap { it.arguments }.filter { it is SkolemTerm && it.isCyclic(targetCyclicTermDepth) }}")
            return false
        }

        DebugLogger.log("number of new facts: ${newNonDatalogFactsInThisIteration.size}")

        // copy list
        newFactsSinceLastApplicabilityCheck = newNonDatalogFactsInThisIteration.toMutableSet()
    } while (!newNonDatalogFactsInThisIteration.isEmpty())

    // if we did not encounter cyclic terms and don't derive anything new anymore,
    // the ruleset is dmfa/rmfa
    return true
}

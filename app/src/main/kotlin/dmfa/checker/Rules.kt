package dmfa.checker

/*
    DMFA Checker
    Rules

    Copyright 2023 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.semanticweb.rulewerk.core.model.api.ExistentialVariable
import org.semanticweb.rulewerk.core.model.api.Predicate
import org.semanticweb.rulewerk.core.model.api.Rule
import org.semanticweb.rulewerk.core.model.api.UniversalVariable
import kotlin.streams.asSequence
import kotlin.streams.toList

val Collection<Rule>.toIndexed: Collection<RuleWithId> get() = mapIndexed { idx, rule -> rule.toRuleWithId(idx) }

val Collection<Rule>.isJoinless: Boolean get() = all { it.isJoinless }
val Collection<Rule>.isLinear: Boolean get() = all { it.isLinear }
val Collection<Rule>.isGuarded: Boolean get() = all { it.isGuarded }

fun Collection<Rule>.checkStickynessWithStartPositions(startPositions: Map<Predicate, Set<Int>>): Boolean {
    var newInLastIteration = startPositions
    var allFoundStickyPositions = startPositions

    do {
        val findableStickyPositions: Map<Predicate, Set<Int>> = fold(mapOf()) { acc, rule ->
            rule.body.conjunctions.fold(acc) { innerAcc, conj ->
                innerAcc.merge(rule.stickyHeadPositionsForBodyVariables(conj.variablesForPositions(newInLastIteration)) ?: return false)
            }
        }

        val newStickyPositions = findableStickyPositions.subtract(allFoundStickyPositions)

        DebugLogger.log("New sticky positions: $newStickyPositions")

        allFoundStickyPositions = allFoundStickyPositions.merge(newStickyPositions)
        newInLastIteration = newStickyPositions
    } while (!newStickyPositions.isEmpty())

    // if sticky positions are saturated without violation, the check succeeds
    return true
}

val Collection<Rule>.isSticky: Boolean get() {
    val stickyPositionsByJoins: Map<Predicate, Set<Int>> = fold(mapOf()) { acc, rule ->
        acc.merge(rule.stickyHeadPositionsForBodyVariables(rule.joinVariables) ?: return false) // if stickyHeadPositions is null, the condition is already violated
    }

    DebugLogger.log("Initial sticky positions: $stickyPositionsByJoins")

    return checkStickynessWithStartPositions(stickyPositionsByJoins)
}

val Collection<Rule>.isDomainRestricted: Boolean get() = all { it.isDomainRestricted }
val Collection<Rule>.isFrontierOne: Boolean get() = all { it.isFrontierOne }
val Collection<Rule>.isDatalog: Boolean get() = all { it.isDatalog }
val Collection<Rule>.isMonadic: Boolean get() = all { it.isMonadic }
val Collection<Rule>.isFrontierGuarded: Boolean get() = all { it.isFrontierGuarded }

val Collection<Rule>.affectedPositions: Map<Predicate, Set<Int>> get() {
    val initialAffectedPositions: Map<Predicate, Set<Int>> = fold(mapOf()) { acc, rule ->
        acc.merge(rule.affectedPositionsBase)
    }

    DebugLogger.log("Initial affected positions: $initialAffectedPositions")

    var newInLastIteration = initialAffectedPositions
    var allFoundAffectedPositions = initialAffectedPositions

    do {
        val findableAffectedPositions: Map<Predicate, Set<Int>> = fold(mapOf()) { acc, rule ->
            rule.body.conjunctions.fold(acc) { innerAcc, conj ->
                innerAcc.merge(rule.affectedPositionsForBodyVariables(conj.variablesForPositions(newInLastIteration)))
            }
        }

        val newAffectedPositions = findableAffectedPositions.subtract(allFoundAffectedPositions)

        DebugLogger.log("New affected positions: $newAffectedPositions")

        allFoundAffectedPositions = allFoundAffectedPositions.merge(newAffectedPositions)
        newInLastIteration = newAffectedPositions
    } while (!newAffectedPositions.isEmpty())

    return allFoundAffectedPositions
}

val Collection<Rule>.isWeaklyGuarded: Boolean get() {
    val affectedPositions = affectedPositions

    return all { it.body.conjunctions.all { it.isGuardedForVars(it.variablesForPositions(affectedPositions)) } }
}

val Collection<Rule>.isWeaklyFrontierGuarded: Boolean get() {
    val affectedPositions = affectedPositions

    return all { rule -> rule.body.conjunctions.all { it.isGuardedForVars(rule.frontier.intersect(it.variablesForPositions(affectedPositions))) } }
}

// https://dl.acm.org/doi/pdf/10.1145/3238304
val Collection<Rule>.isWarded: Boolean get() {
    val affectedPositions = affectedPositions

    return all { rule -> rule.isWardedForPositions(affectedPositions) }
}

val Collection<RuleWithId>.jointlyAffectedPositions: Map<Triple<Int, Int, ExistentialVariable>, Map<Predicate, Set<Int>>> get() =
    fold(mapOf()) { acc, rule ->
        val affectedPositionsForRule: List<Pair<Triple<Int, Int, ExistentialVariable>, Map<Predicate, Set<Int>>>> = rule.head.conjunctions.withIndex().flatMap { (idx, conj) ->
            conj.existentialVariables.asSequence().map { exisVar ->
                val init = conj.positionsOfVariable(exisVar)

                DebugLogger.log("Initial affected positions: $init")

                var newInLastIteration = init
                var allFoundAffectedPositions = init

                do {
                    val findableAffectedPositions: Map<Predicate, Set<Int>> = fold(mapOf()) { acc, rule ->
                        val newPositions: Map<Predicate, Set<Int>> = rule.body.conjunctions
                            // get all variables whose positions in some conjunction are already contained in allFoundAffectedPositions ...
                            .flatMap { it.variablesOnlyAtPositions(allFoundAffectedPositions, newInLastIteration) }.toSet()
                            // ... then add all head positions of the variable
                            .fold(mapOf()) { iAcc, univVar -> iAcc.merge(rule.head.conjunctions.fold(mapOf()) { iiAcc, conj -> iiAcc.merge(conj.positionsOfVariable(univVar)) }) }

                        acc.merge(newPositions)
                    }

                    val newAffectedPositions = findableAffectedPositions.subtract(allFoundAffectedPositions)

                    DebugLogger.log("New affected positions: $newAffectedPositions")

                    allFoundAffectedPositions = allFoundAffectedPositions.merge(newAffectedPositions)
                    newInLastIteration = newAffectedPositions
                } while (!newAffectedPositions.isEmpty())

                Pair(Triple(rule.id, idx, exisVar), allFoundAffectedPositions)
            }
        }

        acc + affectedPositionsForRule
    }

val Collection<RuleWithId>.isJointlyGuarded: Boolean get() {
    val affectedPositionsPerVar = jointlyAffectedPositions

    DebugLogger.log("Affected positions in jointly guarded check: $affectedPositionsPerVar")

    return all {
        it.body.conjunctions.all { conj ->
            val vars: Set<UniversalVariable> = affectedPositionsPerVar.values.fold(setOf()) { acc, variable -> acc + conj.variablesOnlyAtPositions(variable) }

            conj.isGuardedForVars(vars)
        }
    }
}

val Collection<RuleWithId>.isJointlyFrontierGuarded: Boolean get() {
    val affectedPositionsPerVar = jointlyAffectedPositions

    DebugLogger.log("Affected positions in jointly frontier guarded check: $affectedPositionsPerVar")

    return all { rule ->
        rule.body.conjunctions.all { conj ->
            val vars: Set<UniversalVariable> = affectedPositionsPerVar.values.fold(setOf()) { acc, variable -> acc + conj.variablesOnlyAtPositions(variable) }

            conj.isGuardedForVars(rule.frontier.intersect(vars))
        }
    }
}

enum class WAEdgeType {
    NORMAL, SPECIAL
}

val Collection<Rule>.weakAcyclicityGraph: Graph<Pair<Predicate, Int>, WAEdgeType> get() {
    val waGraph = Graph<Pair<Predicate, Int>, WAEdgeType>()

    forEach { rule ->
        val univVars: Set<UniversalVariable> = rule.universalVariables.asSequence().toSet()
        val exisPositions: Set<Pair<Predicate, Int>> = rule.existentialVariables
            .asSequence()
            .flatMap { ex ->
                rule.head.conjunctions.asSequence().flatMap { it.positionsOfVariable(ex).flatMap { (k, v) -> v.map { Pair(k, it) } } }
            }
            .toSet()

        univVars.forEach { un ->
            val bodyPositions: Set<Pair<Predicate, Int>> = rule.body.conjunctions.flatMap { it.positionsOfVariable(un).flatMap { (k, v) -> v.map { Pair(k, it) } } }.toSet()
            val headPositions: Set<Pair<Predicate, Int>> = rule.head.conjunctions.flatMap { it.positionsOfVariable(un).flatMap { (k, v) -> v.map { Pair(k, it) } } }.toSet()

            bodyPositions.forEach { bodyPos ->
                exisPositions.forEach { exPos ->
                    waGraph.addEdge(bodyPos, exPos, WAEdgeType.SPECIAL)
                }

                headPositions.forEach { headPos ->
                    waGraph.addEdge(bodyPos, headPos, WAEdgeType.NORMAL)
                }
            }
        }
    }

    return waGraph
}

val Collection<Rule>.isWeaklyAcyclic: Boolean get() = !weakAcyclicityGraph.hasCycleInvolvingEdgeType(WAEdgeType.SPECIAL)

private fun Collection<RuleWithId>.jointAcyclicityGraphForPositions(jaPositions: Map<Triple<Int, Int, ExistentialVariable>, Map<Predicate, Set<Int>>>): Graph<Triple<Int, Int, ExistentialVariable>, Unit> {
    val jaGraph = Graph<Triple<Int, Int, ExistentialVariable>, Unit>()

    val affected = jaPositions

    filter { it.isGenerating }.forEach { rule ->
        val targets: Set<Triple<Int, Int, ExistentialVariable>> = rule.head.conjunctions.asSequence().flatMapIndexed { disjIdx, conj -> conj.existentialVariables.asSequence().map { Triple(rule.id, disjIdx, it) } }.toSet()

        rule.body.conjunctions.forEach { conj ->
            val sources: Set<Triple<Int, Int, ExistentialVariable>> = conj.universalVariables.asSequence().flatMap {
                val univPositions = conj.positionsOfVariable(it)
                // keep only existential variables whose affected positions are a super set of the positions of the universal variable
                affected.keys.filter { univPositions.subtract(affected.get(it)!!).isEmpty() }
            }.toSet()

            sources.forEach { s ->
                targets.forEach { t ->
                    jaGraph.addEdge(s, t, Unit)
                }
            }
        }
    }

    return jaGraph
}

val Collection<RuleWithId>.jointAcyclicityGraph: Graph<Triple<Int, Int, ExistentialVariable>, Unit> get() = jointAcyclicityGraphForPositions(jointlyAffectedPositions)

val Collection<RuleWithId>.isJointlyAcyclic: Boolean get() = !jointAcyclicityGraph.hasCycleInvolvingEdgeType(Unit)

val Collection<Rule>.isWeaklySticky: Boolean get() {
    val infiniteRankPositions = weakAcyclicityGraph.verticesWithInfiniteRankWithRespectToEdgeType(WAEdgeType.SPECIAL)

    val stickyPositionsByIniniteRankJoinVars: Map<Predicate, Set<Int>> = fold(mapOf()) { acc, rule ->
        val joinVarsWithInfiniteRank = rule.joinVariables.filter { joinVar -> rule.body.conjunctions.any { conj -> conj.positionsOfVariable(joinVar).all { (k, vs) -> vs.all { v -> infiniteRankPositions.contains(Pair(k, v)) } } } }.toSet()

        acc.merge(rule.stickyHeadPositionsForBodyVariables(joinVarsWithInfiniteRank) ?: return false) // if stickyHeadPositions is null, the condition is already violated
    }

    DebugLogger.log("Initial weakly-sticky positions: $stickyPositionsByIniniteRankJoinVars")

    return checkStickynessWithStartPositions(stickyPositionsByIniniteRankJoinVars)
}

val Collection<RuleWithId>.glutPositions: Map<Triple<Int, Int, ExistentialVariable>, Map<Predicate, Set<Int>>> get() {
    val jaPositions = jointlyAffectedPositions
    val graph = jointAcyclicityGraphForPositions(jaPositions)

    val varsInCycles = graph.verticesWithCycleForEdgeType(Unit)

    return jaPositions.filterKeys { it in varsInCycles }
}

val Collection<RuleWithId>.isGlutGuarded: Boolean get() {
    val affectedPositionsPerVar = glutPositions

    DebugLogger.log("Affected positions in glut guarded check: $affectedPositionsPerVar")

    return all {
        it.body.conjunctions.all { conj ->
            val vars: Set<UniversalVariable> = affectedPositionsPerVar.values.fold(setOf()) { acc, variable -> acc + conj.variablesOnlyAtPositions(variable) }

            conj.isGuardedForVars(vars)
        }
    }
}

val Collection<RuleWithId>.isGlutFrontierGuarded: Boolean get() {
    val affectedPositionsPerVar = glutPositions

    DebugLogger.log("Affected positions in glut frontier guarded check: $affectedPositionsPerVar")

    return all { rule ->
        rule.body.conjunctions.all { conj ->
            val vars: Set<UniversalVariable> = affectedPositionsPerVar.values.fold(setOf()) { acc, variable -> acc + conj.variablesOnlyAtPositions(variable) }

            conj.isGuardedForVars(rule.frontier.intersect(vars))
        }
    }
}

val Collection<RuleWithId>.isShy: Boolean get() {
    val jaPositions = jointlyAffectedPositions

    return all { rule ->
        rule.body.conjunctions.all { conj ->
            val univVars = conj.universalVariables.toList()
            val pairsOfVarsToConsider: Sequence<Pair<UniversalVariable, UniversalVariable>> =
                univVars
                    .asSequence()
                    .flatMapIndexed { idx, x ->
                        univVars.asSequence().drop(idx).map { y -> Pair(x, y) }
                    }
                    .filter { (x, y) ->
                        (x == y || rule.head.conjunctions.any { it.universalVariables.toList().containsAll(listOf(x, y)) }) &&
                            conj.literals.withIndex().any { (idx, a) -> a.arguments.contains(x) && conj.literals.drop(idx + 1).any { b -> b.arguments.contains(y) } }
                    }

            DebugLogger.log("Pairs to consider in shy check: ${pairsOfVarsToConsider.toList()}")

            pairsOfVarsToConsider.all { (x, y) -> jaPositions.values.none { conj.positionsOfVariable(x).subtract(it).isEmpty() && conj.positionsOfVariable(y).subtract(it).isEmpty() } }
        }
    }
}

// NOTE: We leave the following out for now since the formal definitions is not quite clear...
// val Collection<Rule>.isStickyJoin: Boolean get() = TODO()
// val Collection<Rule>.isWeaklyStickyJoin: Boolean get() = TODO()

package dmfa.checker

/*
    DMFA Checker
    Blocked Check

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.semanticweb.rulewerk.core.model.api.AbstractConstant
import org.semanticweb.rulewerk.core.model.api.ExistentialVariable
import org.semanticweb.rulewerk.core.model.api.Fact
import org.semanticweb.rulewerk.core.model.api.Predicate
import org.semanticweb.rulewerk.core.model.implementation.Expressions
import kotlin.streams.asSequence

fun isObsolete(rule: RuleWithId, assignment: Assignment, chaseVariant: ChaseVariant, factMap: Map<Predicate, Set<Fact>>): Boolean {
    when (chaseVariant) {
        ChaseVariant.RESTRICTED -> {
            return rule.head.conjunctions.any { conjunction ->
                val assignmentsForHeadConjunction = conjunction.assignmentsForFacts(factMap, factMap, assignment)

                assignmentsForHeadConjunction.any { headAssignment ->
                    rule.frontier.all { assignment.get(it) == headAssignment.get(it) }
                }
            }
        }
        ChaseVariant.SKOLEM -> {
            // 4. check if possible application result is contained in the above result
            val headDisjunctResults = rule.headDisjunctsForAssignment(assignment)

            return headDisjunctResults.any { disjunct ->
                disjunct.all { fact ->
                    fact in factMap.getOrDefault(fact.predicate, setOf())
                }
            }
        }
    }
}

fun isBlocked(rule: RuleWithId, assignment: Assignment, rules: List<RuleWithId>, chaseVariant: ChaseVariant, currentFactMap: Map<Predicate, Set<Fact>>, blockednessNoDatalogClosure: Boolean): Boolean {
    // if a trigger is blocked, it is always obsolete, hence
    // if a trigger is not obsolete it is not blocked
    // BUT we do not take the shortcut if we want to ignore the datalog closure
    if (!blockednessNoDatalogClosure && !isObsolete(rule, assignment, chaseVariant, currentFactMap)) {
        return false
    }

    // 0. remap constants in assignment to unique values
    val uniqueConstantsAssignment: Assignment = assignment.mapValues { (k, v) ->
        v.withUniqueConstants(k.name)
    }

    // System.err.println("skolem terms")

    // 1. get funtional terms (including all subterms)
    val skolemTerms: Sequence<SkolemTerm> = uniqueConstantsAssignment.values
        .asSequence()
        .filterIsInstance<SkolemTerm>()
        .flatMap { it.subterms }

    // 2. get facts necessarily involved in derivation

    // System.err.println("body facts")

    // 2.0 body facts
    val bodyFacts = rule.body.conjunctions.flatMap { it.literals }.map { literal ->
        Expressions.makeFact(
            literal.predicate,
            literal.arguments.map {
                uniqueConstantsAssignment.get(it)!!
            }
        )
    }

    // System.err.println("F_t for blocked check")

    val factsInvolvedInDerivation = skolemTerms.flatMap { term ->
        // 2.1 get rule where each func term comes from
        val ruleForTerm = rules.get(term.ruleId)
        if (ruleForTerm.id != term.ruleId) {
            throw Exception("index should match rule id (we use index to speed up retrieval")
        }

        val headDisjunctIndexForTerm = ruleForTerm.head.conjunctions.indexOfFirst { conjunction ->
            term.variableName in conjunction.existentialVariables.asSequence().map { it.name }
        }

        // 2.2 add body and single head disjunct facts for this rule
        // what to do for same variable name accross multiple disjuncts?
        // -> we rename them apart in the beginning (see DmfaChecker.kt)

        val frontier = ruleForTerm.frontier
        val backtrackAssignment: Assignment = ruleForTerm.universalVariables.asSequence().fold(
            mapOf(),
            { acc, next ->
                val varIndex = frontier.indexOf(next)

                if (varIndex >= 0) {
                    acc + (next to term.arguments.get(varIndex))
                } else {
                    val freshConstant = Expressions.makeAbstractConstant("BLOCK_${next}_$term")
                    acc + (next to freshConstant)
                }
            }
        )

        (
            ruleForTerm.bodyForAssignment(backtrackAssignment).asSequence() +
                ruleForTerm.headDisjunctsForAssignment(backtrackAssignment).get(headDisjunctIndexForTerm).asSequence()
            )
    }

    val allFacts = (bodyFacts + factsInvolvedInDerivation).toSet()

    // 3. datalog closure

    val headPredicates = rule.head.conjunctions.flatMap { it.literals.map { it.predicate } }.toSet()

    if (blockednessNoDatalogClosure) {
        return isObsolete(rule, uniqueConstantsAssignment, chaseVariant, allFacts.predicateMap.filterKeys { it in headPredicates })
    }

    // System.err.println("datalog closure")

    // val datalogReasoner = VlogDatalogReasoner(rules.filter { it.isDatalog }, headPredicates)
    val datalogRules = rules.filter { it.isDatalog }
    val allPredicates = datalogRules.flatMap {
        (
            it.head.conjunctions.flatMap { it.literals.map { it.predicate } } +
                it.body.conjunctions.flatMap { it.literals.map { it.predicate } }
            )
    }.toHashSet() + allFacts.map { it.predicate }.toHashSet()

    val datalogReasoner = ExistentialReasoner(allPredicates, datalogRules, false)
    // System.err.println("datalog closure in blocked check")

    var newInDatalogIteration = allFacts

    while (!newInDatalogIteration.isEmpty()) {
        newInDatalogIteration = datalogReasoner.computeNewFacts(newInDatalogIteration)

        if (isObsolete(rule, uniqueConstantsAssignment, chaseVariant, datalogReasoner.factsPerPredicate.filterKeys { it in headPredicates })) {
            return true
        }
    }

    return isObsolete(rule, uniqueConstantsAssignment, chaseVariant, datalogReasoner.factsPerPredicate.filterKeys { it in headPredicates })
}

object UnblockableCheckCacher {
    var referenceRules: List<RuleWithId>? = null

    var starRules: List<RuleWithId>? = null
    var rulesWithUniqueConstants: List<RuleWithId>? = null
    var criticalInstance: List<Fact>? = null
    var allPredicates: Set<Predicate>? = null
    var initialDatalogClosure: Set<Fact>? = null
}

fun isUnblockable(rule: RuleWithId, assignment: Assignment, rules: List<RuleWithId>, chaseVariant: ChaseVariant, currentFactMap: Map<Predicate, Set<Fact>>, initialFacts: Set<Fact>, unblockabilityUniqueConstants: Boolean, unblockabilityApplicabilityCondition: UnblockabilityApplicabilityCondition, unblockabilityIncludeRuleBodiesWhenBacktracking: Boolean, unblockabilityDisjToConj: Boolean, unblockabilityIgnoreRedundantDerivations: Boolean): Boolean {
    // if a trigger is unblockable, it is never obsolete, hence
    // if a trigger is obsolete it is not unblockable

    // TODO: this shortcut may be wrong in some settings (because we may want to apply triggers that essentially yield the same result as others regardlessly)
    // still, this should only make our results worse currently and it would also only have an impact in weird corner cases (there may be none at all)
    //
    // we should still think about this again...
    if (isObsolete(rule, assignment, chaseVariant, currentFactMap)) {
        return false
    }

    // 0.create new rule set without disjunctions and unique constants instead of exis variales; remap constants in assignment to unique values
    val star = Expressions.makeAbstractConstant("STAR")

    if (UnblockableCheckCacher.referenceRules != rules) {
        val allPredicates = rules.flatMap {
            (
                it.head.conjunctions.flatMap { it.literals.map { it.predicate } } +
                    it.body.conjunctions.flatMap { it.literals.map { it.predicate } }
                )
        }.toHashSet()
        UnblockableCheckCacher.allPredicates = allPredicates

        UnblockableCheckCacher.criticalInstance = criticalInstance(
            allPredicates,
            setOf(Expressions.makeAbstractConstant("STAR")) + initialFacts.flatMap { it.arguments }.filter { it !is SkolemTerm }.map { if (it !is AbstractConstant) throw Exception("only constant (or skolem terms) should occur in initial facts") else it }.toSet()
        )

        if (unblockabilityUniqueConstants) {
            UnblockableCheckCacher.rulesWithUniqueConstants = rules.map { ruleToTransform ->
                // similar to disjunctionsToConjunctions method for rules
                RuleWithId(
                    Expressions.makeRule(
                        Expressions.makeDisjunction(
                            if (unblockabilityDisjToConj) {
                                listOf(
                                    ruleToTransform.head.conjunctions.flatMap { conjunction ->
                                        conjunction.literals.map { literal ->
                                            Expressions.makePositiveLiteral(
                                                literal.predicate,
                                                literal.arguments.map {
                                                    if (it is ExistentialVariable) {
                                                        Expressions.makeAbstractConstant("UNBLOCKABLE_CHECK_RULES_${ruleToTransform.id}_${it.name}")
                                                    } else {
                                                        it
                                                    }
                                                }
                                            )
                                        }
                                    }
                                )
                            } else {
                                ruleToTransform.head.conjunctions.map { conjunction ->
                                    conjunction.literals.map { literal ->
                                        Expressions.makePositiveLiteral(
                                            literal.predicate,
                                            literal.arguments.map {
                                                if (it is ExistentialVariable) {
                                                    Expressions.makeAbstractConstant("UNBLOCKABLE_CHECK_RULES_${ruleToTransform.id}_${it.name}")
                                                } else {
                                                    it
                                                }
                                            }
                                        )
                                    }
                                }
                            }
                        ),
                        ruleToTransform.body
                    ),
                    ruleToTransform.id,
                    if (unblockabilityDisjToConj) null else ruleToTransform.disjunctIndex
                )
            }
        } else {
            UnblockableCheckCacher.starRules = rules.map { ruleToTransform ->
                // similar to disjunctionsToConjunctions method for rules
                RuleWithId(
                    Expressions.makeRule(
                        Expressions.makeDisjunction(
                            if (unblockabilityDisjToConj) {
                                listOf(
                                    ruleToTransform.head.conjunctions.flatMap { conjunction ->
                                        conjunction.literals.map { literal ->
                                            Expressions.makePositiveLiteral(
                                                literal.predicate,
                                                literal.arguments.map {
                                                    if (it is ExistentialVariable) {
                                                        star
                                                    } else {
                                                        it
                                                    }
                                                }
                                            )
                                        }
                                    }
                                )
                            } else {
                                ruleToTransform.head.conjunctions.map { conjunction ->
                                    conjunction.literals.map { literal ->
                                        Expressions.makePositiveLiteral(
                                            literal.predicate,
                                            literal.arguments.map {
                                                if (it is ExistentialVariable) {
                                                    star
                                                } else {
                                                    it
                                                }
                                            }
                                        )
                                    }
                                }
                            }
                        ),
                        ruleToTransform.body
                    ),
                    ruleToTransform.id,
                    if (unblockabilityDisjToConj) null else ruleToTransform.disjunctIndex
                )
            }
        }

        val rulesForInitialDatalogClosure: List<RuleWithId> = if (unblockabilityUniqueConstants) {
            UnblockableCheckCacher.rulesWithUniqueConstants!!
        } else {
            UnblockableCheckCacher.starRules!!
        }

        val initialDatalogReasoner = ExistentialReasoner(allPredicates, rulesForInitialDatalogClosure, false)

        // TODO: if unblockabilityIgnoreRedundantDerivations is set, this should not technically not be closed under datalog rules; I still think that it should not make too much of a difference
        UnblockableCheckCacher.initialDatalogClosure = UnblockableCheckCacher.criticalInstance!!.toSet() + initialDatalogReasoner.computeNewFacts(UnblockableCheckCacher.criticalInstance!!.toSet())

        UnblockableCheckCacher.referenceRules = rules
    }

    val rulesForCheck: List<RuleWithId> = if (unblockabilityUniqueConstants) {
        UnblockableCheckCacher.rulesWithUniqueConstants!!
    } else {
        UnblockableCheckCacher.starRules!!
    }

    val assignmentAdjustedForUnblockabilityCheck: Assignment = assignment.mapValues { (k, v) ->
        if (unblockabilityIncludeRuleBodiesWhenBacktracking || k in rule.frontier) {
            v
        } else if (v is SkolemTerm) {
            // corresponding unique constant for skolem term as introduced for the rule above
            if (unblockabilityUniqueConstants) {
                Expressions.makeAbstractConstant("UNBLOCKABLE_CHECK_RULES_${rule.id}_${k.name}")
            } else {
                star
            }
        } else {
            star
        }
    }

    // 1. construct instance for unblockable check

    val skolemTerms: Sequence<SkolemTerm> = assignmentAdjustedForUnblockabilityCheck.values
        .asSequence()
        .filterIsInstance<SkolemTerm>()
        .flatMap { it.subterms }

    val headFactsInvolvedInDerivation = skolemTerms.flatMap { term ->
        // 2.1 get rule where each func term comes from
        // WE DO NOT USE THE STAR_RULES HERE SINCE WE REQUIRE EXISTENTIAL VARIABLES HERE
        val ruleForTerm = rules.get(term.ruleId)
        if (ruleForTerm.id != term.ruleId) {
            throw Exception("index should match rule id (we use index to speed up retrieval")
        }

        val headDisjunctIndexForTerm = ruleForTerm.head.conjunctions.indexOfFirst { conjunction ->
            term.variableName in conjunction.existentialVariables.asSequence().map { it.name }
        }

        // 2.2 add ONLY single head disjunct facts for this rule
        // what to do for same variable name accross multiple disjuncts?
        // -> we rename them apart in the beginning (see DmfaChecker.kt)

        val frontier = ruleForTerm.frontier
        val backtrackAssignment: Assignment = ruleForTerm.universalVariables.asSequence().fold(
            mapOf(),
            { acc, next ->
                val varIndex = frontier.indexOf(next)

                if (varIndex >= 0) {
                    acc + (next to term.arguments.get(varIndex))
                } else if (unblockabilityIncludeRuleBodiesWhenBacktracking) {
                    acc + (next to star)
                } else { // otherwise ignore non-frontier constants
                    acc
                }
            }
        )

        if (unblockabilityIncludeRuleBodiesWhenBacktracking) {
            ruleForTerm.bodyForAssignment(backtrackAssignment).asSequence() +
                ruleForTerm.headDisjunctsForAssignment(backtrackAssignment).get(headDisjunctIndexForTerm).asSequence()
        } else {
            ruleForTerm.headDisjunctsForAssignment(backtrackAssignment).get(headDisjunctIndexForTerm).asSequence()
        }
    }

    val allStartingFacts = headFactsInvolvedInDerivation.toSet()

    val datalogReasoner = ExistentialReasoner(UnblockableCheckCacher.allPredicates!!, rulesForCheck, false, UnblockableCheckCacher.initialDatalogClosure!!) { datalogRule, datalogAssignment, _ ->
        (!unblockabilityIgnoreRedundantDerivations || !headFactsInvolvedInDerivation.toSet().containsAll(rules[datalogRule.id].headChoiceForAssignment(datalogAssignment).toSet())) &&
            when (unblockabilityApplicabilityCondition) {
                UnblockabilityApplicabilityCondition.EQUIVALENCE_INCLUDING_RULE -> !Pair(rules[datalogRule.id], datalogAssignment).isEqualTo(Pair(rules[rule.id], assignmentAdjustedForUnblockabilityCheck))
                UnblockabilityApplicabilityCondition.EQUIVALENCE_ONLY_RESULT -> !Pair(rules[datalogRule.id], datalogAssignment).yieldsSameResultAs(Pair(rules[rule.id], assignmentAdjustedForUnblockabilityCheck))
                UnblockabilityApplicabilityCondition.SUBSET -> !Pair(rules[rule.id], assignmentAdjustedForUnblockabilityCheck).isProducingSubsetOf(Pair(rules[datalogRule.id], datalogAssignment))
                UnblockabilityApplicabilityCondition.INTERCHANGEABILITY -> !Pair(rules[datalogRule.id], datalogAssignment).isInterchangeableWith(Pair(rules[rule.id], assignmentAdjustedForUnblockabilityCheck))
            }
    }

    val headPredicates = rule.head.conjunctions.flatMap { it.literals.map { it.predicate } }.toSet()

    var newInDatalogIteration = allStartingFacts

    while (!newInDatalogIteration.isEmpty()) {
        newInDatalogIteration = datalogReasoner.computeNewFacts(newInDatalogIteration)

        if (isObsolete(rule, assignmentAdjustedForUnblockabilityCheck, chaseVariant, datalogReasoner.factsPerPredicate.filterKeys { it in headPredicates })) {
            return false
        }
    }

    return !isObsolete(rule, assignmentAdjustedForUnblockabilityCheck, chaseVariant, datalogReasoner.factsPerPredicate.filterKeys { it in headPredicates })
}

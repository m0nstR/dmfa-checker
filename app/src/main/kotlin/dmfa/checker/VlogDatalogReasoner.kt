package dmfa.checker

/*
    DMFA Checker
    Datalog Reasoner
    (multiple ones)

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.semanticweb.rulewerk.core.model.api.Fact
import org.semanticweb.rulewerk.core.model.api.PositiveLiteral
import org.semanticweb.rulewerk.core.model.api.Predicate
import org.semanticweb.rulewerk.core.model.api.Rule
import org.semanticweb.rulewerk.core.model.api.Term
import org.semanticweb.rulewerk.core.model.implementation.Expressions
import org.semanticweb.rulewerk.core.reasoner.Algorithm
import org.semanticweb.rulewerk.core.reasoner.KnowledgeBase
import org.semanticweb.rulewerk.reasoner.vlog.VLogReasoner

class VlogDatalogReasoner(val datalogRules: List<Rule>, answerPredicates: Set<Predicate>? = null) {
    val datalogKb = KnowledgeBase()
    val queries: List<PositiveLiteral>
    val vlogReasoner: VLogReasoner

    val datalogPredicates = datalogRules.flatMap {
        (
            it.head.conjunctions.flatMap { it.literals.map { it.predicate } } +
                it.body.conjunctions.flatMap { it.literals.map { it.predicate } }
            )
    }.toSet()

    // redundant facts storage outside of datalogKb
    // because it seems to be cheaper to recompute facts in datalogKb than to add them beforehand
    // for some reason this affects the time taken for querying new facts...
    val factsHashSet: HashSet<Fact> = hashSetOf()

    init {
        datalogKb.addStatements(datalogRules)
        vlogReasoner = VLogReasoner(datalogKb)
        // vlogReasoner.setLogLevel(LogLevel.DEBUG)
        vlogReasoner.setAlgorithm(Algorithm.SKOLEM_CHASE)

        val queryPredicates = if (answerPredicates != null) { answerPredicates } else { datalogPredicates }

        queries = queryPredicates.map {
            Expressions.makePositiveLiteral(it, (1..it.arity).map { Expressions.makeUniversalVariable("X$it") })
        }
    }

    fun computeNewDatalogFacts(
        newFacts: Set<Fact>
    ): Set<Fact> {
        val filteredNewFacts = newFacts.filter { it.predicate in datalogPredicates }

        // factsHashSet contains all facts whereas datalogKb contains only new facts
        // from existential rules since recomputing datalog facts seems to be faster here
        // for some reason
        datalogKb.addStatements(filteredNewFacts)
        factsHashSet.addAll(filteredNewFacts)

        val termsBeforeMap = factsHashSet.flatMap { it.arguments }.toHashSet().fold(
            hashMapOf<String, Term>(),
            { acc, next ->
                acc.put(next.name, next)
                acc
            }
        )

        vlogReasoner.reason()

        DebugLogger.log("reasoning finished")

        val newDatalogFacts = hashSetOf<Fact>()

        DebugLogger.log("queries: ${queries.size}")
        queries.forEach { query ->
            // System.err.println("query start")
            // System.err.println(query.predicate)
            val answers = vlogReasoner.answerQuery(query, true)
            // System.err.println("query end")

            answers.forEach { answer ->
                val mappedBackTerms = answer.terms.map { vlogTerm ->
                    termsBeforeMap.get(vlogTerm.name) ?: Expressions.makeAbstractConstant(vlogTerm.name)
                }

                val mappedBackFact = Expressions.makeFact(query.predicate, mappedBackTerms)

                if (mappedBackFact !in factsHashSet) {
                    newDatalogFacts.add(mappedBackFact)
                }
            }
        }

        factsHashSet.addAll(newDatalogFacts)
        // datalogKb.addStatements(newDatalogFacts)

        return newDatalogFacts
    }
}

// class EnrichedDatalogReasoner(allRules: List<RuleWithId>) {
//     val basicReasoner: VlogDatalogReasoner
//     val mapBackAnswerPredicates: Map<Predicate, Predicate>

//     init {
//         val initialPredicates: Set<Predicate> = allRules.flatMap {
//             (
//                 it.head.conjunctions.flatMap { it.literals.map { it.predicate } } +
//                     it.body.conjunctions.flatMap { it.literals.map { it.predicate } }
//                 )
//         }.toSet()

//         val existingPredicateNames = initialPredicates.map { it.name }

//         val datalogRules = allRules.filter { it.isDatalog }

//         if ("NEW_TERM" in existingPredicateNames) {
//             throw Exception("predicate name NEW_TERM is already taken; we use this as auxiliary name so it should not occur in input rule set")
//         }

//         val answerPredicates = mutableSetOf<Predicate>()

//         // NOTE: talk about that in evaluation section(?)

//         val enrichedDatalogRules = datalogRules
//             .flatMap { rule ->
//                 rule.body.universalVariables.toList().map { outerVariable ->
//                     Expressions.makeRule(
//                         Expressions.makeDisjunction(
//                             rule.head.conjunctions.map { conjunction ->
//                                 conjunction.literals.flatMap { literal ->
//                                     val answerPredicate = literal.predicate.answerPredicate

//                                     if (answerPredicate.name in existingPredicateNames) {
//                                         throw Exception("predicate name ${answerPredicate.name} is already taken; this is highly unlikely to happen")
//                                     }

//                                     // ATTENTION: we have an intentional side effect here!
//                                     // otherwise we would have to pass this all the way to the outside
//                                     // or compute it seperately again afterwards
//                                     // which seems unnecessarily complicated
//                                     answerPredicates.add(answerPredicate)

//                                     listOf(
//                                         literal,
//                                         Expressions.makePositiveLiteral(
//                                             answerPredicate,
//                                             literal.arguments
//                                         )
//                                     )
//                                 } + conjunction.universalVariables.toList().map { variable ->
//                                     Expressions.makePositiveLiteral(
//                                         "NEW_TERM",
//                                         variable
//                                     )
//                                 }
//                             }
//                         ),
//                         Expressions.makeDisjunction(
//                             rule.body.conjunctions.map { conjunction ->
//                                 conjunction.literals + Expressions.makePositiveLiteral(
//                                     "NEW_TERM",
//                                     outerVariable
//                                 )
//                             }
//                         )
//                     )
//                 }
//             }

//         basicReasoner = VlogDatalogReasoner(enrichedDatalogRules, answerPredicates)
//         mapBackAnswerPredicates = initialPredicates.map { it.answerPredicate to it }.toMap()

//         DebugLogger.log("datalogRules: ${datalogRules.size}")
//         DebugLogger.log("enrichedDatalogRules: ${enrichedDatalogRules.size}")
//     }

//     fun computeNewDatalogFacts(
//         newFacts: Set<Fact>
//     ): Set<Fact> {
//         val termsBefore = basicReasoner.datalogKb.facts.flatMap { it.arguments }.toHashSet()

//         val newTermStatements = newFacts
//             .flatMap { fact ->
//                 fact.arguments
//             }
//             .toSet()
//             .filter {
//                 it !in termsBefore
//             }
//             .map { term ->
//                 Expressions.makeFact(
//                     "NEW_TERM",
//                     term
//                 )
//             }

//         // System.err.println(newTermStatements.size)

//         basicReasoner.datalogKb.addStatements(newTermStatements)

//         val newDatalogFacts = basicReasoner.computeNewDatalogFacts(newFacts)
//             .map { fact ->
//                 Expressions.makeFact(mapBackAnswerPredicates.get(fact.predicate)!!, fact.arguments)
//             }.toSet()

//         basicReasoner.datalogKb.removeStatements(newTermStatements)
//         // System.err.println("remapping finished")

//         basicReasoner.datalogKb.addStatements(newDatalogFacts)

//         return newDatalogFacts
//     }
// }

package dmfa.checker

/*
    DMFA Checker
    DMFA Checker Test

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory
import org.semanticweb.rulewerk.core.reasoner.KnowledgeBase
import org.semanticweb.rulewerk.parser.RuleParser
import java.io.File
import kotlin.test.assertEquals

// compatibility for old tests
fun checkDmfa(kb: KnowledgeBase) = checkAcyclicity(kb.rules, ChaseVariant.SKOLEM, 1, false)
fun checkDmfa2(kb: KnowledgeBase) = checkAcyclicity(kb.rules, ChaseVariant.SKOLEM, 5, false)
fun checkRmfa(kb: KnowledgeBase) = checkAcyclicity(kb.rules, ChaseVariant.RESTRICTED, 1, false)
fun checkRmfa2(kb: KnowledgeBase) = checkAcyclicity(kb.rules, ChaseVariant.RESTRICTED, 5, false)
fun checkDmfc(kb: KnowledgeBase) = checkCyclicity(kb.rules, ChaseVariant.SKOLEM, true, false, UnblockabilityApplicabilityCondition.EQUIVALENCE_ONLY_RESULT, false, false, false)
fun checkDmfcAndIgnoreRedundancies(kb: KnowledgeBase) = checkCyclicity(kb.rules, ChaseVariant.SKOLEM, true, false, UnblockabilityApplicabilityCondition.EQUIVALENCE_ONLY_RESULT, false, false, true)
fun checkDmfcWithUniqueConstantAndSubsetCheckAndBacktrackingBodies(kb: KnowledgeBase) = checkCyclicity(kb.rules, ChaseVariant.SKOLEM, true, true, UnblockabilityApplicabilityCondition.SUBSET, true, false, false)
fun checkDmfcWithUniqueConstantAndInterchangeability(kb: KnowledgeBase) = checkCyclicity(kb.rules, ChaseVariant.SKOLEM, true, true, UnblockabilityApplicabilityCondition.INTERCHANGEABILITY, false, false, false)
fun checkRmfc(kb: KnowledgeBase) = checkCyclicity(kb.rules, ChaseVariant.RESTRICTED, false, false, UnblockabilityApplicabilityCondition.EQUIVALENCE_INCLUDING_RULE, false, false, false)
fun checkDrmfc(kb: KnowledgeBase) = checkCyclicity(kb.rules, ChaseVariant.RESTRICTED, true, true, UnblockabilityApplicabilityCondition.INTERCHANGEABILITY, false, false, false)
fun checkDrmfcAndIgnoreRedundancies(kb: KnowledgeBase) = checkCyclicity(kb.rules, ChaseVariant.RESTRICTED, true, true, UnblockabilityApplicabilityCondition.INTERCHANGEABILITY, false, false, true)
fun checkRpc(kb: KnowledgeBase) = checkCyclicity(kb.rules, ChaseVariant.RESTRICTED, true, true, UnblockabilityApplicabilityCondition.EQUIVALENCE_ONLY_RESULT, false, false, true)

fun createTest(name: String, target: Boolean, check: (KnowledgeBase) -> Boolean): List<DynamicTest> = File("../test-data/is${if (target) { name } else { "Not$name" }}/").walkTopDown()
    .filter { """.+\.rules$""".toRegex().matches(it.name) }
    .map { file ->
        dynamicTest("correctly check that ${file.path} is ${if (target) { name } else { "not $name" }}") {
            val kb = RuleParser.parse(file.inputStream())

            val computed = check(kb)

            assertEquals(target, computed)
        }
    }
    .toList()

class DmfaCheckerTest {
    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeJoinless() = createTest("Joinless", true) { it.rules.isJoinless }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeJoinless() = createTest("Joinless", false) { it.rules.isJoinless }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeLinear() = createTest("Linear", true) { it.rules.isLinear }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeLinear() = createTest("Linear", false) { it.rules.isLinear }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeGuarded() = createTest("Guarded", true) { it.rules.isGuarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeGuarded() = createTest("Guarded", false) { it.rules.isGuarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeSticky() = createTest("Sticky", true) { it.rules.isSticky }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeSticky() = createTest("Sticky", false) { it.rules.isSticky }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeDomainRestricted() = createTest("DomainRestricted", true) { it.rules.isDomainRestricted }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeDomainRestricted() = createTest("DomainRestricted", false) { it.rules.isDomainRestricted }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeFrontierOne() = createTest("FrontierOne", true) { it.rules.isFrontierOne }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeFrontierOne() = createTest("FrontierOne", false) { it.rules.isFrontierOne }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeDatalog() = createTest("Datalog", true) { it.rules.isDatalog }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeDatalog() = createTest("Datalog", false) { it.rules.isDatalog }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeMonadic() = createTest("Monadic", true) { it.rules.isMonadic }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeMonadic() = createTest("Monadic", false) { it.rules.isMonadic }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeFrontierGuarded() = createTest("FrontierGuarded", true) { it.rules.isFrontierGuarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeFrontierGuarded() = createTest("FrontierGuarded", false) { it.rules.isFrontierGuarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeWeaklyGuarded() = createTest("WeaklyGuarded", true) { it.rules.isWeaklyGuarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeWeaklyGuarded() = createTest("WeaklyGuarded", false) { it.rules.isWeaklyGuarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeWeaklyFrontierGuarded() = createTest("WeaklyFrontierGuarded", true) { it.rules.isWeaklyFrontierGuarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeWeaklyFrontierGuarded() = createTest("WeaklyFrontierGuarded", false) { it.rules.isWeaklyFrontierGuarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeWarded() = createTest("Warded", true) { it.rules.isWarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeWarded() = createTest("Warded", false) { it.rules.isWarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeJointlyGuarded() = createTest("JointlyGuarded", true) { it.rules.toIndexed.isJointlyGuarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeJointlyGuarded() = createTest("JointlyGuarded", false) { it.rules.toIndexed.isJointlyGuarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeJointlyFrontierGuarded() = createTest("JointlyFrontierGuarded", true) { it.rules.toIndexed.isJointlyFrontierGuarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeJointlyFrontierGuarded() = createTest("JointlyFrontierGuarded", false) { it.rules.toIndexed.isJointlyFrontierGuarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeWeaklyAcyclic() = createTest("WeaklyAcyclic", true) { it.rules.isWeaklyAcyclic }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeWeaklyAcyclic() = createTest("WeaklyAcyclic", false) { it.rules.isWeaklyAcyclic }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeJointlyAcyclic() = createTest("JointlyAcyclic", true) { it.rules.toIndexed.isJointlyAcyclic }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeJointlyAcyclic() = createTest("JointlyAcyclic", false) { it.rules.toIndexed.isJointlyAcyclic }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeWeaklySticky() = createTest("WeaklySticky", true) { it.rules.isWeaklySticky }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeWeaklySticky() = createTest("WeaklySticky", false) { it.rules.isWeaklySticky }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeGlutGuarded() = createTest("GlutGuarded", true) { it.rules.toIndexed.isGlutGuarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeGlutGuarded() = createTest("GlutGuarded", false) { it.rules.toIndexed.isGlutGuarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeGlutFrontierGuarded() = createTest("GlutFrontierGuarded", true) { it.rules.toIndexed.isGlutFrontierGuarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeGlutFrontierGuarded() = createTest("GlutFrontierGuarded", false) { it.rules.toIndexed.isGlutFrontierGuarded }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeShy() = createTest("Shy", true) { it.rules.toIndexed.isShy }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeShy() = createTest("Shy", false) { it.rules.toIndexed.isShy }

    // Chase-like checks start here
    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeDmfa() = createTest("Dmfa", true) { checkDmfa(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeDmfa() = createTest("Dmfa", false) { checkDmfa(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeDmfa2() = createTest("Dmfa2", true) { checkDmfa2(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeDmfa2() = createTest("Dmfa2", false) { checkDmfa2(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeRmfa() = createTest("Rmfa", true) { checkRmfa(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeRmfa() = createTest("Rmfa", false) { checkRmfa(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeRmfa2() = createTest("Rmfa2", true) { checkRmfa2(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeRmfa2() = createTest("Rmfa2", false) { checkRmfa2(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeDmfc() = createTest("Dmfc", true) { checkDmfc(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeDmfc() = createTest("Dmfc", false) { checkDmfc(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeDmfcWhileIgnoringRedundanciesInUnblockability() = createTest("Dmfc", true) { checkDmfcAndIgnoreRedundancies(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeDmfcWhileIgnoringRedundanciesInUnblockability() = createTest("Dmfc", false) { checkDmfcAndIgnoreRedundancies(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeDmfcWithUniqueConstantAndSubsetCheckAndBacktrackingBodies() = createTest("Dmfc", true) { checkDmfcWithUniqueConstantAndSubsetCheckAndBacktrackingBodies(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeDmfcWithUniqueConstantAndSubsetCheckAndBacktrackingBodies() = createTest("Dmfc", false) { checkDmfcWithUniqueConstantAndSubsetCheckAndBacktrackingBodies(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeDmfcWithUniqueConstantAndInterchangeability() = createTest("Dmfc", true) { checkDmfcWithUniqueConstantAndInterchangeability(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeDmfcWithUniqueConstantAndInterchangeability() = createTest("Dmfc", false) { checkDmfcWithUniqueConstantAndInterchangeability(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeRmfc() = createTest("Rmfc", true) { checkRmfc(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeRmfc() = createTest("Rmfc", false) { checkRmfc(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeDrmfc() = createTest("Drmfc", true) { checkDrmfc(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeDrmfc() = createTest("Drmfc", false) { checkDrmfc(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeDrmfcWhileIgnoringRedundanciesInUnblockability() = createTest("Drmfc", true) { checkDrmfcAndIgnoreRedundancies(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeDrmfcWhileIgnoringRedundanciesInUnblockability() = createTest("Drmfc", false) { checkDrmfcAndIgnoreRedundancies(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldBeRpc() = createTest("Rpc", true) { checkRpc(it) }

    @TestFactory
    fun testAllInTestDataDirectoryThatShouldNotBeRpc() = createTest("Rpc", false) { checkRpc(it) }
}
